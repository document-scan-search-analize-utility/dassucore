package com.golemteam.dassu.core.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceUploadCompositResponse;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

public interface FileStorageService {
    void init();

    CompletableFuture<BinaryFile> storeComposite(MultipartFile file, String categoryId) throws IOException;

    Resource loadAsResource(String filename);

    ResponseEntity<RecognitionServiceUploadCompositResponse> handleFileUpload(BinaryFile fileRequest) throws JsonProcessingException;

}
