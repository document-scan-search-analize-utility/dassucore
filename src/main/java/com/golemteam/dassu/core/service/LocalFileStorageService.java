package com.golemteam.dassu.core.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.golemteam.dassu.core.config.ApplicationProperties;
import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.domain.CompositFile;
import com.golemteam.dassu.core.domain.FileCategory;
import com.golemteam.dassu.core.domain.enumeration.ProcessingStatus;
import com.golemteam.dassu.core.exception.StorageException;
import com.golemteam.dassu.core.exception.StorageFileNotFoundException;
import com.golemteam.dassu.core.repository.BinaryFileRepository;
import com.golemteam.dassu.core.repository.CompositFileRepository;
import com.golemteam.dassu.core.repository.FileCategoryRepository;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceUploadCompositResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.CompletableFuture;

import static com.golemteam.dassu.core.Utils.*;

@Service
public class LocalFileStorageService implements FileStorageService {

    private final Path rootLocation;
    private final Logger log = LoggerFactory.getLogger(LocalFileStorageService.class);

    private final ApplicationProperties properties;
    private final FileCategoryRepository fileCategoryRepository;
    private final CompositFileRepository compositFileRepository;
    private final BinaryFileRepository binaryFileRepository;

    private final ObjectMapper objectMapper;

    public LocalFileStorageService(ApplicationProperties properties,
                                   FileCategoryRepository fileCategoryRepository,
                                   CompositFileRepository compositFileRepository,
                                   BinaryFileRepository binaryFileRepository,
                                   ObjectMapper objectMapper) {
        this.properties = properties;
        this.rootLocation = Paths.get(properties.getLocation());

        this.fileCategoryRepository = fileCategoryRepository;
        this.compositFileRepository = compositFileRepository;
        this.binaryFileRepository = binaryFileRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public void init() {
        try {
            if (!rootLocation.toFile().exists()) {
                Files.createDirectories(rootLocation);
            }
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public CompletableFuture<BinaryFile> storeComposite(MultipartFile file, String categoryId) throws IOException {
        log.info("\tSTART store(file: {}, category: {})", file.getName(), categoryId);
        File localStorageDir = new File(properties.getLocation());

        CompositFile compositFile = compositFileRepository.save(new CompositFile());
        BinaryFile binaryFile = binaryFileRepository.save(new BinaryFile());

        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        mkDirs(localStorageDir, compositFile.getId().toString());
        File compositVersionDir = new File(properties.getLocation() + File.separator + compositFile.getId().toString());
        mkDirs(compositVersionDir, compositFile.getId().toString());

        Path fileLocation = Paths.get(rootLocation.toString(), compositFile.getId().toString());

        checkIsFileCorrect(file);

        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, fileLocation.resolve(filename),
                StandardCopyOption.REPLACE_EXISTING);
            log.debug("\t\tfile saved");
        } catch (IOException e) {
            log.error("\t\tFailed to store file {}", filename, e);
            throw new StorageException("Failed to store file " + filename, e);
        }

        File resultFile = new File(fileLocation + File.separator + filename);
        String hash = getMd5(resultFile);

        compositFile.setBinaryFile(binaryFile);
        setCategoryForCompositFile(compositFile, categoryId);
        compositFile.setStatus(ProcessingStatus.CREATED);
        compositFileRepository.save(compositFile);

        binaryFile.setMd5(hash);
        binaryFile.setComposit(compositFile);
        binaryFile.setName(file.getOriginalFilename());
        binaryFile.setStorageFilePath(localStorageDir.getCanonicalPath() + File.separator + compositFile.getId().toString() + File.separator + file.getOriginalFilename());
        binaryFile.setVersion(binaryFileRepository.getMaxVersion(hash) + 1);
        binaryFileRepository.save(binaryFile);

        return CompletableFuture.completedFuture(binaryFile);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException(
                    "Could not read file: " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    private void checkIsFileCorrect(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        if (file.isEmpty()) {
            log.debug("\t\tFailed to store empty file {}", filename);
            throw new StorageException("Failed to store empty file " + filename);
        }
        if (file.getOriginalFilename().contains("..")) {
            log.error("\t Security check failed");
            throw new StorageException(
                "Cannot store file with relative path outside current dictionary "
                    + filename);
        }
    }

    private void setCategoryForCompositFile(CompositFile file, String categoryId) {
        FileCategory category;
        if (categoryId != null && !categoryId.isEmpty()) {
            try {
                category = fileCategoryRepository.getOne(Long.parseLong(categoryId));
                file.setFileCategory(category);
            } catch (EntityNotFoundException e) {
                log.error("category with id " + categoryId + " not found", e);
            }
        }
        compositFileRepository.save(file);
    }

    @Override
    public ResponseEntity<RecognitionServiceUploadCompositResponse> handleFileUpload(BinaryFile binaryFile) throws JsonProcessingException {
        HttpHeaders fileHeaders = new HttpHeaders();
        fileHeaders.add("accept", "*/*");
        fileHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        String url = properties.getRecognitionServiceUri() + "/upload";
        File file = new File(Paths.get(binaryFile.getStorageFilePath()).toUri());

        if (file.exists()) {
            FileSystemResource res = new FileSystemResource(file);
            MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
            params.add("file", res);
            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(params, fileHeaders);

            RestTemplate restTemplate = getRestTemplate();
            binaryFile.getComposit().setStatus(ProcessingStatus.PROCESSING);
            ResponseEntity<RecognitionServiceUploadCompositResponse> exchange = restTemplate.exchange(url, HttpMethod.POST, requestEntity, RecognitionServiceUploadCompositResponse.class, "");

            log.debug("resp from service: {}", exchange.getBody());

            binaryFile.getComposit().setPreProcessingData(objectMapper.writeValueAsString(exchange.getBody()));
            compositFileRepository.save(binaryFile.getComposit());

            return exchange;
        }
        return null;
    }
}
