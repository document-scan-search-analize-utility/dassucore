package com.golemteam.dassu.core.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.golemteam.dassu.core.config.ApplicationProperties;
import com.golemteam.dassu.core.domain.AtomicFile;
import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.domain.CompositFile;
import com.golemteam.dassu.core.domain.enumeration.ProcessingStatus;
import com.golemteam.dassu.core.repository.AtomicFileRepository;
import com.golemteam.dassu.core.repository.BinaryFileRepository;
import com.golemteam.dassu.core.repository.CompositFileRepository;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceAnnotatedDataResponse;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceUploadCompositResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.golemteam.dassu.core.Utils.*;

@Service
public class RecognitionServiceRequester {

    private final Logger log = LoggerFactory.getLogger(RecognitionServiceRequester.class);

    private final ApplicationProperties properties;
    private final CompositFileRepository compositFileRepository;
    private final AtomicFileRepository atomicFileRepository;
    private final BinaryFileRepository binaryFileRepository;

    private final Path rootLocation;

    private ObjectMapper objectMapper = new ObjectMapper();

    public RecognitionServiceRequester(ApplicationProperties properties, CompositFileRepository compositFileRepository, AtomicFileRepository atomicFileRepository, BinaryFileRepository binaryFileRepository) {
        this.properties = properties;
        this.compositFileRepository = compositFileRepository;
        this.atomicFileRepository = atomicFileRepository;
        this.rootLocation = Paths.get(properties.getLocation());
        this.binaryFileRepository = binaryFileRepository;
    }

    public void requestForDocumentPredictResults(String documentId) {
        HttpHeaders jsonHeaders = new HttpHeaders();
        jsonHeaders.add("accept", "*/*");
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(jsonHeaders);

        RestTemplate template = getRestTemplate();
        template.postForEntity(properties.getRecognitionServiceUri() + "/" + documentId + "/predict", requestEntity, String.class);
    }

    @Scheduled(fixedRate  = 180000) // every 5 minutes
    @Transactional
    public void documentPredictionRequester() throws IOException {
        log.debug("documentPredictionRequester started");

        for(CompositFile file : compositFileRepository.getAllByStatusEquals(ProcessingStatus.PROCESSING)) {
            RecognitionServiceUploadCompositResponse fileData = objectMapper.readValue(file.getPreProcessingData(), RecognitionServiceUploadCompositResponse.class);
            for(String page : fileData.getPages()) {
                String url = properties.getRecognitionServiceUri()
                    + "/" + fileData.getDocumentId()
                    + "/pages/" + page;
                RecognitionServiceAnnotatedDataResponse body = getRestTemplate().getForEntity(url, RecognitionServiceAnnotatedDataResponse.class).getBody();
                assert body != null;

                AtomicFile image = new AtomicFile();
                atomicFileRepository.save(image);

                File imageFile = base64ToPngImageFile(body.getImg().split(",")[1]);

                String storageFilePath = file.getBinaryFile().getStorageFilePath()
                    .replace(file.getBinaryFile().getName(), file.getId().toString());

                Path copied = Paths.get(storageFilePath + File.separator + image.getId() + ".png");
                Path copy = Files.copy(imageFile.toPath(), copied, StandardCopyOption.REPLACE_EXISTING);

                BinaryFile binaryImage = new BinaryFile();
                binaryImage
                    .atomic(image)
                    .md5(getMd5(copy.toFile()))
                    .name(image.getId().toString())
                    .storageFilePath(copy.toFile().getCanonicalPath());

                image
                    .annotatedData(objectMapper.writeValueAsString(body.getAnnotatedData()))
                    .parent(file)
                    .binaryFile(binaryImage);

                binaryFileRepository.save(binaryImage);
                atomicFileRepository.save(image);

                log.debug("documentPredictionRequester for img base64: {}", body.getImg());
            }

            log.debug("\tdocumentServiceResponse: {}", objectMapper.writeValueAsString(fileData));
            file.setStatus(ProcessingStatus.PARSED);
            compositFileRepository.save(file);
        }

    }

}
