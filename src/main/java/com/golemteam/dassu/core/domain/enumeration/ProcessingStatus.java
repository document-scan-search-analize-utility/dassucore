package com.golemteam.dassu.core.domain.enumeration;

/**
 * The ProcessingStatus enumeration.
 */
public enum ProcessingStatus {
    CREATED, PROCESSING, PARSED
}
