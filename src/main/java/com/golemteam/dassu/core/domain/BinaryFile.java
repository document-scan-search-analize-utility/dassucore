package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

import com.golemteam.dassu.core.domain.enumeration.StorageType;

/**
 * A BinaryFile.
 */
@Entity
@Table(name = "binary_file")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "binaryfile")
public class BinaryFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "version")
    private Long version;

    @Column(name = "storage_file_path")
    private String storageFilePath;

    @Column(name = "md_5")
    private String md5;

    @Enumerated(EnumType.STRING)
    @Column(name = "storage_type")
    private StorageType storageType;

    @OneToOne(mappedBy = "binaryFile")
    @JsonIgnore
    private AtomicFile atomic;

    @OneToOne(mappedBy = "binaryFile")
    @JsonIgnore
    private CompositFile composit;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public BinaryFile name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getVersion() {
        return version;
    }

    public BinaryFile version(Long version) {
        this.version = version;
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getStorageFilePath() {
        return storageFilePath;
    }

    public BinaryFile storageFilePath(String storageFilePath) {
        this.storageFilePath = storageFilePath;
        return this;
    }

    public void setStorageFilePath(String storageFilePath) {
        this.storageFilePath = storageFilePath;
    }

    public String getMd5() {
        return md5;
    }

    public BinaryFile md5(String md5) {
        this.md5 = md5;
        return this;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public StorageType getStorageType() {
        return storageType;
    }

    public BinaryFile storageType(StorageType storageType) {
        this.storageType = storageType;
        return this;
    }

    public void setStorageType(StorageType storageType) {
        this.storageType = storageType;
    }

    public AtomicFile getAtomic() {
        return atomic;
    }

    public BinaryFile atomic(AtomicFile atomicFile) {
        this.atomic = atomicFile;
        return this;
    }

    public void setAtomic(AtomicFile atomicFile) {
        this.atomic = atomicFile;
    }

    public CompositFile getComposit() {
        return composit;
    }

    public BinaryFile composit(CompositFile compositFile) {
        this.composit = compositFile;
        return this;
    }

    public void setComposit(CompositFile compositFile) {
        this.composit = compositFile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BinaryFile)) {
            return false;
        }
        return id != null && id.equals(((BinaryFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BinaryFile{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", version=" + getVersion() +
            ", storageFilePath='" + getStorageFilePath() + "'" +
            ", md5='" + getMd5() + "'" +
            ", storageType='" + getStorageType() + "'" +
            "}";
    }
}
