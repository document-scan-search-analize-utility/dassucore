package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A Metric.
 */
@Entity
@Table(name = "metric")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "metric")
public class Metric implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "key")
    private String key;

    @Column(name = "value")
    private Float value;

    @OneToOne(mappedBy = "metric")
    @JsonIgnore
    private FileCategory fileCategory;

    @OneToOne(mappedBy = "metric")
    @JsonIgnore
    private FileCategoryFieldValue fileCategoryFieldValue;

    @ManyToOne
    @JsonIgnoreProperties(value = "metrics", allowSetters = true)
    private MetricType metricType;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public Metric key(String key) {
        this.key = key;
        return this;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Float getValue() {
        return value;
    }

    public Metric value(Float value) {
        this.value = value;
        return this;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public FileCategory getFileCategory() {
        return fileCategory;
    }

    public Metric fileCategory(FileCategory fileCategory) {
        this.fileCategory = fileCategory;
        return this;
    }

    public void setFileCategory(FileCategory fileCategory) {
        this.fileCategory = fileCategory;
    }

    public FileCategoryFieldValue getFileCategoryFieldValue() {
        return fileCategoryFieldValue;
    }

    public Metric fileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
        return this;
    }

    public void setFileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public Metric metricType(MetricType metricType) {
        this.metricType = metricType;
        return this;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Metric)) {
            return false;
        }
        return id != null && id.equals(((Metric) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Metric{" +
            "id=" + getId() +
            ", key='" + getKey() + "'" +
            ", value=" + getValue() +
            "}";
    }
}
