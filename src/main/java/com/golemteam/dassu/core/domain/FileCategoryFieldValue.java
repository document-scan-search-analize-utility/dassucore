package com.golemteam.dassu.core.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;

/**
 * A FileCategoryFieldValue.
 */
@Entity
@Table(name = "file_category_field_value")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "filecategoryfieldvalue")
public class FileCategoryFieldValue implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "value")
    private String value;

    @OneToOne
    @JoinColumn(unique = true)
    private CompositFile compositFile;

    @OneToOne
    @JoinColumn(unique = true)
    private FileCategoryField fileCategoryField;

    @OneToOne
    @JoinColumn(unique = true)
    private Metric metric;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public FileCategoryFieldValue value(String value) {
        this.value = value;
        return this;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CompositFile getCompositFile() {
        return compositFile;
    }

    public FileCategoryFieldValue compositFile(CompositFile compositFile) {
        this.compositFile = compositFile;
        return this;
    }

    public void setCompositFile(CompositFile compositFile) {
        this.compositFile = compositFile;
    }

    public FileCategoryField getFileCategoryField() {
        return fileCategoryField;
    }

    public FileCategoryFieldValue fileCategoryField(FileCategoryField fileCategoryField) {
        this.fileCategoryField = fileCategoryField;
        return this;
    }

    public void setFileCategoryField(FileCategoryField fileCategoryField) {
        this.fileCategoryField = fileCategoryField;
    }

    public Metric getMetric() {
        return metric;
    }

    public FileCategoryFieldValue metric(Metric metric) {
        this.metric = metric;
        return this;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileCategoryFieldValue)) {
            return false;
        }
        return id != null && id.equals(((FileCategoryFieldValue) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FileCategoryFieldValue{" +
            "id=" + getId() +
            ", value='" + getValue() + "'" +
            "}";
    }
}
