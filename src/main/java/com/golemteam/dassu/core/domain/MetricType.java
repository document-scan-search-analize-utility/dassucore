package com.golemteam.dassu.core.domain;


import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A MetricType.
 */
@Entity
@Table(name = "metric_type")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "metrictype")
public class MetricType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "metricType")
    private Set<Metric> metrics = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public MetricType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Metric> getMetrics() {
        return metrics;
    }

    public MetricType metrics(Set<Metric> metrics) {
        this.metrics = metrics;
        return this;
    }

    public MetricType addMetrics(Metric metric) {
        this.metrics.add(metric);
        metric.setMetricType(this);
        return this;
    }

    public MetricType removeMetrics(Metric metric) {
        this.metrics.remove(metric);
        metric.setMetricType(null);
        return this;
    }

    public void setMetrics(Set<Metric> metrics) {
        this.metrics = metrics;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MetricType)) {
            return false;
        }
        return id != null && id.equals(((MetricType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MetricType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
