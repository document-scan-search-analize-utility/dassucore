package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.golemteam.dassu.core.domain.enumeration.ProcessingStatus;

/**
 * A CompositFile.
 */
@Entity
@Table(name = "composit_file")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "compositfile")
public class CompositFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ProcessingStatus status;

    @Column(name = "pre_processing_data")
    private String preProcessingData;

    @OneToOne
    @JoinColumn(unique = true)
    private BinaryFile binaryFile;

    @OneToMany(mappedBy = "parent")
    private Set<AtomicFile> atomics = new HashSet<>();

    @OneToOne(mappedBy = "compositFile")
    @JsonIgnore
    private FileCategoryFieldValue fileCategoryFieldValue;

    @ManyToOne
    @JsonIgnoreProperties(value = "composits", allowSetters = true)
    private FileCategory fileCategory;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProcessingStatus getStatus() {
        return status;
    }

    public CompositFile status(ProcessingStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(ProcessingStatus status) {
        this.status = status;
    }

    public String getPreProcessingData() {
        return preProcessingData;
    }

    public CompositFile preProcessingData(String preProcessingData) {
        this.preProcessingData = preProcessingData;
        return this;
    }

    public void setPreProcessingData(String preProcessingData) {
        this.preProcessingData = preProcessingData;
    }

    public BinaryFile getBinaryFile() {
        return binaryFile;
    }

    public CompositFile binaryFile(BinaryFile binaryFile) {
        this.binaryFile = binaryFile;
        return this;
    }

    public void setBinaryFile(BinaryFile binaryFile) {
        this.binaryFile = binaryFile;
    }

    public Set<AtomicFile> getAtomics() {
        return atomics;
    }

    public CompositFile atomics(Set<AtomicFile> atomicFiles) {
        this.atomics = atomicFiles;
        return this;
    }

    public CompositFile addAtomics(AtomicFile atomicFile) {
        this.atomics.add(atomicFile);
        atomicFile.setParent(this);
        return this;
    }

    public CompositFile removeAtomics(AtomicFile atomicFile) {
        this.atomics.remove(atomicFile);
        atomicFile.setParent(null);
        return this;
    }

    public void setAtomics(Set<AtomicFile> atomicFiles) {
        this.atomics = atomicFiles;
    }

    public FileCategoryFieldValue getFileCategoryFieldValue() {
        return fileCategoryFieldValue;
    }

    public CompositFile fileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
        return this;
    }

    public void setFileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
    }

    public FileCategory getFileCategory() {
        return fileCategory;
    }

    public CompositFile fileCategory(FileCategory fileCategory) {
        this.fileCategory = fileCategory;
        return this;
    }

    public void setFileCategory(FileCategory fileCategory) {
        this.fileCategory = fileCategory;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CompositFile)) {
            return false;
        }
        return id != null && id.equals(((CompositFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CompositFile{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", preProcessingData='" + getPreProcessingData() + "'" +
            "}";
    }
}
