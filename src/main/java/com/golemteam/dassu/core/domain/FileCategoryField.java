package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A FileCategoryField.
 */
@Entity
@Table(name = "file_category_field")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "filecategoryfield")
public class FileCategoryField implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "meta")
    private String meta;

    @ManyToMany
    @JoinTable(name = "file_category_field_file_category",
               joinColumns = @JoinColumn(name = "file_category_field_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "file_category_id", referencedColumnName = "id"))
    private Set<FileCategory> fileCategories = new HashSet<>();

    @OneToOne(mappedBy = "fileCategoryField")
    @JsonIgnore
    private FileCategoryFieldValue fileCategoryFieldValue;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public FileCategoryField name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeta() {
        return meta;
    }

    public FileCategoryField meta(String meta) {
        this.meta = meta;
        return this;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public Set<FileCategory> getFileCategories() {
        return fileCategories;
    }

    public FileCategoryField fileCategories(Set<FileCategory> fileCategories) {
        this.fileCategories = fileCategories;
        return this;
    }

    public FileCategoryField addFileCategory(FileCategory fileCategory) {
        this.fileCategories.add(fileCategory);
        fileCategory.getFileCategoryFields().add(this);
        return this;
    }

    public FileCategoryField removeFileCategory(FileCategory fileCategory) {
        this.fileCategories.remove(fileCategory);
        fileCategory.getFileCategoryFields().remove(this);
        return this;
    }

    public void setFileCategories(Set<FileCategory> fileCategories) {
        this.fileCategories = fileCategories;
    }

    public FileCategoryFieldValue getFileCategoryFieldValue() {
        return fileCategoryFieldValue;
    }

    public FileCategoryField fileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
        return this;
    }

    public void setFileCategoryFieldValue(FileCategoryFieldValue fileCategoryFieldValue) {
        this.fileCategoryFieldValue = fileCategoryFieldValue;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileCategoryField)) {
            return false;
        }
        return id != null && id.equals(((FileCategoryField) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FileCategoryField{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", meta='" + getMeta() + "'" +
            "}";
    }
}
