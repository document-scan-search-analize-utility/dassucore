package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import java.io.Serializable;

/**
 * A AtomicFile.
 */
@Entity
@Table(name = "atomic_file")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "atomicfile")
public class AtomicFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "annotated_data")
    private String annotatedData;

    @Column(name = "annotated_text")
    private String annotatedText;

    @OneToOne
    @JoinColumn(unique = true)
    private BinaryFile binaryFile;

    @ManyToOne
    @JsonIgnoreProperties(value = "atomics", allowSetters = true)
    private CompositFile parent;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnnotatedData() {
        return annotatedData;
    }

    public AtomicFile annotatedData(String annotatedData) {
        this.annotatedData = annotatedData;
        return this;
    }

    public void setAnnotatedData(String annotatedData) {
        this.annotatedData = annotatedData;
    }

    public String getAnnotatedText() {
        return annotatedText;
    }

    public AtomicFile annotatedText(String annotatedText) {
        this.annotatedText = annotatedText;
        return this;
    }

    public void setAnnotatedText(String annotatedText) {
        this.annotatedText = annotatedText;
    }

    public BinaryFile getBinaryFile() {
        return binaryFile;
    }

    public AtomicFile binaryFile(BinaryFile binaryFile) {
        this.binaryFile = binaryFile;
        return this;
    }

    public void setBinaryFile(BinaryFile binaryFile) {
        this.binaryFile = binaryFile;
    }

    public CompositFile getParent() {
        return parent;
    }

    public AtomicFile parent(CompositFile compositFile) {
        this.parent = compositFile;
        return this;
    }

    public void setParent(CompositFile compositFile) {
        this.parent = compositFile;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AtomicFile)) {
            return false;
        }
        return id != null && id.equals(((AtomicFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AtomicFile{" +
            "id=" + getId() +
            ", annotatedData='" + getAnnotatedData() + "'" +
            ", annotatedText='" + getAnnotatedText() + "'" +
            "}";
    }
}
