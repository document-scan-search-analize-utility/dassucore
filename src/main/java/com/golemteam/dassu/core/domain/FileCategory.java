package com.golemteam.dassu.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * description contains of classification data (type)\nand all fields
 */
@ApiModel(description = "description contains of classification data (type)\nand all fields")
@Entity
@Table(name = "file_category")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "filecategory")
public class FileCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(unique = true)
    private Metric metric;

    @OneToMany(mappedBy = "fileCategory")
    private Set<CompositFile> composits = new HashSet<>();

    @ManyToMany(mappedBy = "fileCategories")
    @JsonIgnore
    private Set<FileCategoryField> fileCategoryFields = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public FileCategory name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public FileCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Metric getMetric() {
        return metric;
    }

    public FileCategory metric(Metric metric) {
        this.metric = metric;
        return this;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public Set<CompositFile> getComposits() {
        return composits;
    }

    public FileCategory composits(Set<CompositFile> compositFiles) {
        this.composits = compositFiles;
        return this;
    }

    public FileCategory addComposits(CompositFile compositFile) {
        this.composits.add(compositFile);
        compositFile.setFileCategory(this);
        return this;
    }

    public FileCategory removeComposits(CompositFile compositFile) {
        this.composits.remove(compositFile);
        compositFile.setFileCategory(null);
        return this;
    }

    public void setComposits(Set<CompositFile> compositFiles) {
        this.composits = compositFiles;
    }

    public Set<FileCategoryField> getFileCategoryFields() {
        return fileCategoryFields;
    }

    public FileCategory fileCategoryFields(Set<FileCategoryField> fileCategoryFields) {
        this.fileCategoryFields = fileCategoryFields;
        return this;
    }

    public FileCategory addFileCategoryField(FileCategoryField fileCategoryField) {
        this.fileCategoryFields.add(fileCategoryField);
        fileCategoryField.getFileCategories().add(this);
        return this;
    }

    public FileCategory removeFileCategoryField(FileCategoryField fileCategoryField) {
        this.fileCategoryFields.remove(fileCategoryField);
        fileCategoryField.getFileCategories().remove(this);
        return this;
    }

    public void setFileCategoryFields(Set<FileCategoryField> fileCategoryFields) {
        this.fileCategoryFields = fileCategoryFields;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileCategory)) {
            return false;
        }
        return id != null && id.equals(((FileCategory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FileCategory{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
