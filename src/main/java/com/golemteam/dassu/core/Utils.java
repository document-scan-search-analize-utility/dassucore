package com.golemteam.dassu.core;

import com.google.common.hash.Hashing;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

public class Utils {

    public static String getMd5(File file) throws IOException {
        return com.google.common.io.Files
            .asByteSource(file)
            .hash(Hashing.goodFastHash(128)).toString().toUpperCase();
    }

    /**
     * Mk dirs.
     *
     * @param outdir the outdir
     * @param path   the path
     */
    public static void mkDirs(File outdir, String path) {
        File d = new File(outdir, path);
        if (!d.exists()) {
            d.mkdirs();
        }
    }

    public static File base64ToPngImageFile(String base64String) throws IOException {
        byte[] imageByte = Base64.decodeBase64(base64String);
        File outputfile = new File("image.png");

        FileUtils.writeByteArrayToFile(outputfile, imageByte);
        return outputfile;
    }

    public static RestTemplate getRestTemplate() {
        RestTemplateBuilder builder = new RestTemplateBuilder();
        return builder.setConnectTimeout(Duration.ofMinutes(10)).setReadTimeout(Duration.ofMinutes(10)).build();
    }
}
