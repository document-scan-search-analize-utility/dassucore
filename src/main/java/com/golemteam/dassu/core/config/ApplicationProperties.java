package com.golemteam.dassu.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to Dassu Core.
 * <p>
 * Properties are configured in the {@code application.yml} file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private String location;
    private int bufferSize;
    private String recognitionServiceUri;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public ApplicationProperties setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
        return this;
    }

    public String getRecognitionServiceUri() {
        return recognitionServiceUri;
    }

    public void setRecognitionServiceUri(String recognitionServiceUri) {
        this.recognitionServiceUri = recognitionServiceUri;
    }

}
