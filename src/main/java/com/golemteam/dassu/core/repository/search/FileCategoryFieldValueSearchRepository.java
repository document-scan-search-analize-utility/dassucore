package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.FileCategoryFieldValue;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link FileCategoryFieldValue} entity.
 */
public interface FileCategoryFieldValueSearchRepository extends ElasticsearchRepository<FileCategoryFieldValue, Long> {
}
