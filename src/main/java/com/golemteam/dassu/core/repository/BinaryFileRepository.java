package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.BinaryFile;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BinaryFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BinaryFileRepository extends JpaRepository<BinaryFile, Long> {

    @Query(
        nativeQuery = true,
        value = "select\n" +
            "    case\n" +
            "        when MAX(version) ISNULL\n" +
            "            then 0\n" +
            "        else MAX(version)\n" +
            "    end\n" +
            "from binary_file\n" +
            "where :hash = md_5"
    )
    Long getMaxVersion(@Param("hash") String hash);

}
