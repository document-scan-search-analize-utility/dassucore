package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.FileCategoryField;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link FileCategoryField} entity.
 */
public interface FileCategoryFieldSearchRepository extends ElasticsearchRepository<FileCategoryField, Long> {
}
