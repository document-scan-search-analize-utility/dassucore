package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.MetricType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link MetricType} entity.
 */
public interface MetricTypeSearchRepository extends ElasticsearchRepository<MetricType, Long> {
}
