package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.AtomicFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link AtomicFile} entity.
 */
public interface AtomicFileSearchRepository extends ElasticsearchRepository<AtomicFile, Long> {
}
