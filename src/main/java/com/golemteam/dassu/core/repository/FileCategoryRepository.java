package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.FileCategory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FileCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileCategoryRepository extends JpaRepository<FileCategory, Long> {
}
