package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.CompositFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link CompositFile} entity.
 */
public interface CompositFileSearchRepository extends ElasticsearchRepository<CompositFile, Long> {
}
