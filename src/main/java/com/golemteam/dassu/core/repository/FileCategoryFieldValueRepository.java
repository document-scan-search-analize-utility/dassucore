package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.FileCategoryFieldValue;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the FileCategoryFieldValue entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FileCategoryFieldValueRepository extends JpaRepository<FileCategoryFieldValue, Long> {
}
