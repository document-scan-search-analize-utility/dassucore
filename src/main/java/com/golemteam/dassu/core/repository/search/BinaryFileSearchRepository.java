package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.BinaryFile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link BinaryFile} entity.
 */
public interface BinaryFileSearchRepository extends ElasticsearchRepository<BinaryFile, Long> {
}
