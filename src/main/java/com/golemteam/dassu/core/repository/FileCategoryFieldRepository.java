package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.FileCategoryField;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the FileCategoryField entity.
 */
@Repository
public interface FileCategoryFieldRepository extends JpaRepository<FileCategoryField, Long> {

    @Query(value = "select distinct fileCategoryField from FileCategoryField fileCategoryField left join fetch fileCategoryField.fileCategories",
        countQuery = "select count(distinct fileCategoryField) from FileCategoryField fileCategoryField")
    Page<FileCategoryField> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct fileCategoryField from FileCategoryField fileCategoryField left join fetch fileCategoryField.fileCategories")
    List<FileCategoryField> findAllWithEagerRelationships();

    @Query("select fileCategoryField from FileCategoryField fileCategoryField left join fetch fileCategoryField.fileCategories where fileCategoryField.id =:id")
    Optional<FileCategoryField> findOneWithEagerRelationships(@Param("id") Long id);
}
