package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.AtomicFile;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the AtomicFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AtomicFileRepository extends JpaRepository<AtomicFile, Long> {
}
