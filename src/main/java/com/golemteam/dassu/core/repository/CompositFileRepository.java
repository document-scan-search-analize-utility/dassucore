package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.CompositFile;

import com.golemteam.dassu.core.domain.enumeration.ProcessingStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * Spring Data  repository for the CompositFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CompositFileRepository extends JpaRepository<CompositFile, Long> {

    Set<CompositFile> getAllByStatusEquals(ProcessingStatus status);
}
