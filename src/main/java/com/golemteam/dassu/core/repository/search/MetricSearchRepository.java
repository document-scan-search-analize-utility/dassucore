package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.Metric;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link Metric} entity.
 */
public interface MetricSearchRepository extends ElasticsearchRepository<Metric, Long> {
}
