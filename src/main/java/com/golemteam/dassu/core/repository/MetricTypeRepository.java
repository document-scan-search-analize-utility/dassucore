package com.golemteam.dassu.core.repository;

import com.golemteam.dassu.core.domain.MetricType;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the MetricType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MetricTypeRepository extends JpaRepository<MetricType, Long> {
}
