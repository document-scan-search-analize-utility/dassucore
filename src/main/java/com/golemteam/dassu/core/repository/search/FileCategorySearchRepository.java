package com.golemteam.dassu.core.repository.search;

import com.golemteam.dassu.core.domain.FileCategory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


/**
 * Spring Data Elasticsearch repository for the {@link FileCategory} entity.
 */
public interface FileCategorySearchRepository extends ElasticsearchRepository<FileCategory, Long> {
}
