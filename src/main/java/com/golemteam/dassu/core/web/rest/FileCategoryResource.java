package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.FileCategory;
import com.golemteam.dassu.core.repository.FileCategoryRepository;
import com.golemteam.dassu.core.repository.search.FileCategorySearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.FileCategory}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FileCategoryResource {

    private final Logger log = LoggerFactory.getLogger(FileCategoryResource.class);

    private static final String ENTITY_NAME = "dassuCoreFileCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FileCategoryRepository fileCategoryRepository;

    private final FileCategorySearchRepository fileCategorySearchRepository;

    public FileCategoryResource(FileCategoryRepository fileCategoryRepository, FileCategorySearchRepository fileCategorySearchRepository) {
        this.fileCategoryRepository = fileCategoryRepository;
        this.fileCategorySearchRepository = fileCategorySearchRepository;
    }

    /**
     * {@code POST  /file-categories} : Create a new fileCategory.
     *
     * @param fileCategory the fileCategory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fileCategory, or with status {@code 400 (Bad Request)} if the fileCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/file-categories")
    public ResponseEntity<FileCategory> createFileCategory(@RequestBody FileCategory fileCategory) throws URISyntaxException {
        log.debug("REST request to save FileCategory : {}", fileCategory);
        if (fileCategory.getId() != null) {
            throw new BadRequestAlertException("A new fileCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FileCategory result = fileCategoryRepository.save(fileCategory);
        fileCategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/file-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /file-categories} : Updates an existing fileCategory.
     *
     * @param fileCategory the fileCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fileCategory,
     * or with status {@code 400 (Bad Request)} if the fileCategory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fileCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/file-categories")
    public ResponseEntity<FileCategory> updateFileCategory(@RequestBody FileCategory fileCategory) throws URISyntaxException {
        log.debug("REST request to update FileCategory : {}", fileCategory);
        if (fileCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FileCategory result = fileCategoryRepository.save(fileCategory);
        fileCategorySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fileCategory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /file-categories} : get all the fileCategories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fileCategories in body.
     */
    @GetMapping("/file-categories")
    public List<FileCategory> getAllFileCategories() {
        log.debug("REST request to get all FileCategories");
        return fileCategoryRepository.findAll();
    }

    /**
     * {@code GET  /file-categories/:id} : get the "id" fileCategory.
     *
     * @param id the id of the fileCategory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fileCategory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/file-categories/{id}")
    public ResponseEntity<FileCategory> getFileCategory(@PathVariable Long id) {
        log.debug("REST request to get FileCategory : {}", id);
        Optional<FileCategory> fileCategory = fileCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fileCategory);
    }

    /**
     * {@code DELETE  /file-categories/:id} : delete the "id" fileCategory.
     *
     * @param id the id of the fileCategory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/file-categories/{id}")
    public ResponseEntity<Void> deleteFileCategory(@PathVariable Long id) {
        log.debug("REST request to delete FileCategory : {}", id);
        fileCategoryRepository.deleteById(id);
        fileCategorySearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/file-categories?query=:query} : search for the fileCategory corresponding
     * to the query.
     *
     * @param query the query of the fileCategory search.
     * @return the result of the search.
     */
    @GetMapping("/_search/file-categories")
    public List<FileCategory> searchFileCategories(@RequestParam String query) {
        log.debug("REST request to search FileCategories for query {}", query);
        return StreamSupport
            .stream(fileCategorySearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
