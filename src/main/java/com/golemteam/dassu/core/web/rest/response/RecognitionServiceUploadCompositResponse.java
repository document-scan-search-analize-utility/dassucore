package com.golemteam.dassu.core.web.rest.response;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.List;

public class RecognitionServiceUploadCompositResponse {
    @JsonAlias(value = "document_id")
    private String documentId;

    private Long category;

    private List<String> pages;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Long getCategory() {
        return category;
    }

    public void setCategory(Long category) {
        this.category = category;
    }

    public List<String> getPages() {
        return pages;
    }

    public void setPages(List<String> pages) {
        this.pages = pages;
    }
}
