package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.FileCategoryField;
import com.golemteam.dassu.core.repository.FileCategoryFieldRepository;
import com.golemteam.dassu.core.repository.search.FileCategoryFieldSearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.FileCategoryField}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FileCategoryFieldResource {

    private final Logger log = LoggerFactory.getLogger(FileCategoryFieldResource.class);

    private static final String ENTITY_NAME = "dassuCoreFileCategoryField";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FileCategoryFieldRepository fileCategoryFieldRepository;

    private final FileCategoryFieldSearchRepository fileCategoryFieldSearchRepository;

    public FileCategoryFieldResource(FileCategoryFieldRepository fileCategoryFieldRepository, FileCategoryFieldSearchRepository fileCategoryFieldSearchRepository) {
        this.fileCategoryFieldRepository = fileCategoryFieldRepository;
        this.fileCategoryFieldSearchRepository = fileCategoryFieldSearchRepository;
    }

    /**
     * {@code POST  /file-category-fields} : Create a new fileCategoryField.
     *
     * @param fileCategoryField the fileCategoryField to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fileCategoryField, or with status {@code 400 (Bad Request)} if the fileCategoryField has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/file-category-fields")
    public ResponseEntity<FileCategoryField> createFileCategoryField(@RequestBody FileCategoryField fileCategoryField) throws URISyntaxException {
        log.debug("REST request to save FileCategoryField : {}", fileCategoryField);
        if (fileCategoryField.getId() != null) {
            throw new BadRequestAlertException("A new fileCategoryField cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FileCategoryField result = fileCategoryFieldRepository.save(fileCategoryField);
        fileCategoryFieldSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/file-category-fields/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /file-category-fields} : Updates an existing fileCategoryField.
     *
     * @param fileCategoryField the fileCategoryField to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fileCategoryField,
     * or with status {@code 400 (Bad Request)} if the fileCategoryField is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fileCategoryField couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/file-category-fields")
    public ResponseEntity<FileCategoryField> updateFileCategoryField(@RequestBody FileCategoryField fileCategoryField) throws URISyntaxException {
        log.debug("REST request to update FileCategoryField : {}", fileCategoryField);
        if (fileCategoryField.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FileCategoryField result = fileCategoryFieldRepository.save(fileCategoryField);
        fileCategoryFieldSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fileCategoryField.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /file-category-fields} : get all the fileCategoryFields.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fileCategoryFields in body.
     */
    @GetMapping("/file-category-fields")
    public List<FileCategoryField> getAllFileCategoryFields(@RequestParam(required = false) String filter,@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        if ("filecategoryfieldvalue-is-null".equals(filter)) {
            log.debug("REST request to get all FileCategoryFields where fileCategoryFieldValue is null");
            return StreamSupport
                .stream(fileCategoryFieldRepository.findAll().spliterator(), false)
                .filter(fileCategoryField -> fileCategoryField.getFileCategoryFieldValue() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all FileCategoryFields");
        return fileCategoryFieldRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /file-category-fields/:id} : get the "id" fileCategoryField.
     *
     * @param id the id of the fileCategoryField to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fileCategoryField, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/file-category-fields/{id}")
    public ResponseEntity<FileCategoryField> getFileCategoryField(@PathVariable Long id) {
        log.debug("REST request to get FileCategoryField : {}", id);
        Optional<FileCategoryField> fileCategoryField = fileCategoryFieldRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(fileCategoryField);
    }

    /**
     * {@code DELETE  /file-category-fields/:id} : delete the "id" fileCategoryField.
     *
     * @param id the id of the fileCategoryField to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/file-category-fields/{id}")
    public ResponseEntity<Void> deleteFileCategoryField(@PathVariable Long id) {
        log.debug("REST request to delete FileCategoryField : {}", id);
        fileCategoryFieldRepository.deleteById(id);
        fileCategoryFieldSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/file-category-fields?query=:query} : search for the fileCategoryField corresponding
     * to the query.
     *
     * @param query the query of the fileCategoryField search.
     * @return the result of the search.
     */
    @GetMapping("/_search/file-category-fields")
    public List<FileCategoryField> searchFileCategoryFields(@RequestParam String query) {
        log.debug("REST request to search FileCategoryFields for query {}", query);
        return StreamSupport
            .stream(fileCategoryFieldSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
