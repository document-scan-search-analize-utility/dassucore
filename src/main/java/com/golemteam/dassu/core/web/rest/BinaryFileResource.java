package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.repository.BinaryFileRepository;
import com.golemteam.dassu.core.repository.search.BinaryFileSearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.BinaryFile}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BinaryFileResource {

    private final Logger log = LoggerFactory.getLogger(BinaryFileResource.class);

    private static final String ENTITY_NAME = "dassuCoreBinaryFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BinaryFileRepository binaryFileRepository;

    private final BinaryFileSearchRepository binaryFileSearchRepository;

    public BinaryFileResource(BinaryFileRepository binaryFileRepository, BinaryFileSearchRepository binaryFileSearchRepository) {
        this.binaryFileRepository = binaryFileRepository;
        this.binaryFileSearchRepository = binaryFileSearchRepository;
    }

    /**
     * {@code POST  /binary-files} : Create a new binaryFile.
     *
     * @param binaryFile the binaryFile to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new binaryFile, or with status {@code 400 (Bad Request)} if the binaryFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/binary-files")
    public ResponseEntity<BinaryFile> createBinaryFile(@RequestBody BinaryFile binaryFile) throws URISyntaxException {
        log.debug("REST request to save BinaryFile : {}", binaryFile);
        if (binaryFile.getId() != null) {
            throw new BadRequestAlertException("A new binaryFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BinaryFile result = binaryFileRepository.save(binaryFile);
        binaryFileSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/binary-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /binary-files} : Updates an existing binaryFile.
     *
     * @param binaryFile the binaryFile to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated binaryFile,
     * or with status {@code 400 (Bad Request)} if the binaryFile is not valid,
     * or with status {@code 500 (Internal Server Error)} if the binaryFile couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/binary-files")
    public ResponseEntity<BinaryFile> updateBinaryFile(@RequestBody BinaryFile binaryFile) throws URISyntaxException {
        log.debug("REST request to update BinaryFile : {}", binaryFile);
        if (binaryFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BinaryFile result = binaryFileRepository.save(binaryFile);
        binaryFileSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, binaryFile.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /binary-files} : get all the binaryFiles.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of binaryFiles in body.
     */
    @GetMapping("/binary-files")
    public List<BinaryFile> getAllBinaryFiles(@RequestParam(required = false) String filter) {
        if ("atomic-is-null".equals(filter)) {
            log.debug("REST request to get all BinaryFiles where atomic is null");
            return StreamSupport
                .stream(binaryFileRepository.findAll().spliterator(), false)
                .filter(binaryFile -> binaryFile.getAtomic() == null)
                .collect(Collectors.toList());
        }
        if ("composit-is-null".equals(filter)) {
            log.debug("REST request to get all BinaryFiles where composit is null");
            return StreamSupport
                .stream(binaryFileRepository.findAll().spliterator(), false)
                .filter(binaryFile -> binaryFile.getComposit() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all BinaryFiles");
        return binaryFileRepository.findAll();
    }

    /**
     * {@code GET  /binary-files/:id} : get the "id" binaryFile.
     *
     * @param id the id of the binaryFile to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the binaryFile, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/binary-files/{id}")
    public ResponseEntity<BinaryFile> getBinaryFile(@PathVariable Long id) {
        log.debug("REST request to get BinaryFile : {}", id);
        Optional<BinaryFile> binaryFile = binaryFileRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(binaryFile);
    }

    /**
     * {@code DELETE  /binary-files/:id} : delete the "id" binaryFile.
     *
     * @param id the id of the binaryFile to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/binary-files/{id}")
    public ResponseEntity<Void> deleteBinaryFile(@PathVariable Long id) {
        log.debug("REST request to delete BinaryFile : {}", id);
        binaryFileRepository.deleteById(id);
        binaryFileSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/binary-files?query=:query} : search for the binaryFile corresponding
     * to the query.
     *
     * @param query the query of the binaryFile search.
     * @return the result of the search.
     */
    @GetMapping("/_search/binary-files")
    public List<BinaryFile> searchBinaryFiles(@RequestParam String query) {
        log.debug("REST request to search BinaryFiles for query {}", query);
        return StreamSupport
            .stream(binaryFileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
