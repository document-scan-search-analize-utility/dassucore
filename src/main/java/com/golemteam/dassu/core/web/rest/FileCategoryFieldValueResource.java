package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.FileCategoryFieldValue;
import com.golemteam.dassu.core.repository.FileCategoryFieldValueRepository;
import com.golemteam.dassu.core.repository.search.FileCategoryFieldValueSearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.FileCategoryFieldValue}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class FileCategoryFieldValueResource {

    private final Logger log = LoggerFactory.getLogger(FileCategoryFieldValueResource.class);

    private static final String ENTITY_NAME = "dassuCoreFileCategoryFieldValue";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FileCategoryFieldValueRepository fileCategoryFieldValueRepository;

    private final FileCategoryFieldValueSearchRepository fileCategoryFieldValueSearchRepository;

    public FileCategoryFieldValueResource(FileCategoryFieldValueRepository fileCategoryFieldValueRepository, FileCategoryFieldValueSearchRepository fileCategoryFieldValueSearchRepository) {
        this.fileCategoryFieldValueRepository = fileCategoryFieldValueRepository;
        this.fileCategoryFieldValueSearchRepository = fileCategoryFieldValueSearchRepository;
    }

    /**
     * {@code POST  /file-category-field-values} : Create a new fileCategoryFieldValue.
     *
     * @param fileCategoryFieldValue the fileCategoryFieldValue to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new fileCategoryFieldValue, or with status {@code 400 (Bad Request)} if the fileCategoryFieldValue has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/file-category-field-values")
    public ResponseEntity<FileCategoryFieldValue> createFileCategoryFieldValue(@RequestBody FileCategoryFieldValue fileCategoryFieldValue) throws URISyntaxException {
        log.debug("REST request to save FileCategoryFieldValue : {}", fileCategoryFieldValue);
        if (fileCategoryFieldValue.getId() != null) {
            throw new BadRequestAlertException("A new fileCategoryFieldValue cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FileCategoryFieldValue result = fileCategoryFieldValueRepository.save(fileCategoryFieldValue);
        fileCategoryFieldValueSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/file-category-field-values/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /file-category-field-values} : Updates an existing fileCategoryFieldValue.
     *
     * @param fileCategoryFieldValue the fileCategoryFieldValue to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated fileCategoryFieldValue,
     * or with status {@code 400 (Bad Request)} if the fileCategoryFieldValue is not valid,
     * or with status {@code 500 (Internal Server Error)} if the fileCategoryFieldValue couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/file-category-field-values")
    public ResponseEntity<FileCategoryFieldValue> updateFileCategoryFieldValue(@RequestBody FileCategoryFieldValue fileCategoryFieldValue) throws URISyntaxException {
        log.debug("REST request to update FileCategoryFieldValue : {}", fileCategoryFieldValue);
        if (fileCategoryFieldValue.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FileCategoryFieldValue result = fileCategoryFieldValueRepository.save(fileCategoryFieldValue);
        fileCategoryFieldValueSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, fileCategoryFieldValue.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /file-category-field-values} : get all the fileCategoryFieldValues.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of fileCategoryFieldValues in body.
     */
    @GetMapping("/file-category-field-values")
    public List<FileCategoryFieldValue> getAllFileCategoryFieldValues() {
        log.debug("REST request to get all FileCategoryFieldValues");
        return fileCategoryFieldValueRepository.findAll();
    }

    /**
     * {@code GET  /file-category-field-values/:id} : get the "id" fileCategoryFieldValue.
     *
     * @param id the id of the fileCategoryFieldValue to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the fileCategoryFieldValue, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/file-category-field-values/{id}")
    public ResponseEntity<FileCategoryFieldValue> getFileCategoryFieldValue(@PathVariable Long id) {
        log.debug("REST request to get FileCategoryFieldValue : {}", id);
        Optional<FileCategoryFieldValue> fileCategoryFieldValue = fileCategoryFieldValueRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(fileCategoryFieldValue);
    }

    /**
     * {@code DELETE  /file-category-field-values/:id} : delete the "id" fileCategoryFieldValue.
     *
     * @param id the id of the fileCategoryFieldValue to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/file-category-field-values/{id}")
    public ResponseEntity<Void> deleteFileCategoryFieldValue(@PathVariable Long id) {
        log.debug("REST request to delete FileCategoryFieldValue : {}", id);
        fileCategoryFieldValueRepository.deleteById(id);
        fileCategoryFieldValueSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/file-category-field-values?query=:query} : search for the fileCategoryFieldValue corresponding
     * to the query.
     *
     * @param query the query of the fileCategoryFieldValue search.
     * @return the result of the search.
     */
    @GetMapping("/_search/file-category-field-values")
    public List<FileCategoryFieldValue> searchFileCategoryFieldValues(@RequestParam String query) {
        log.debug("REST request to search FileCategoryFieldValues for query {}", query);
        return StreamSupport
            .stream(fileCategoryFieldValueSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
