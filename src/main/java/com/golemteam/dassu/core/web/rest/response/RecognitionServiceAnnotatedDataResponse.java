package com.golemteam.dassu.core.web.rest.response;

import java.util.List;

public class RecognitionServiceAnnotatedDataResponse {

    private AnnotatedData annotatedData;
    private String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public AnnotatedData getAnnotatedData() {
        return annotatedData;
    }

    public void setAnnotatedData(AnnotatedData annotatedData) {
        this.annotatedData = annotatedData;
    }

    public static class AnnotatedData {
        private List<Data> data;

        public List<Data> getData() {
            return data;
        }

        public void setData(List<Data> data) {
            this.data = data;
        }

        public static class Data {
            private Integer bottom;
            private Integer left;
            private Integer right;
            private Integer top;
            private Integer id;
            private String text;

            public Integer getBottom() {
                return bottom;
            }

            public void setBottom(Integer bottom) {
                this.bottom = bottom;
            }

            public Integer getLeft() {
                return left;
            }

            public void setLeft(Integer left) {
                this.left = left;
            }

            public Integer getRight() {
                return right;
            }

            public void setRight(Integer right) {
                this.right = right;
            }

            public Integer getTop() {
                return top;
            }

            public void setTop(Integer top) {
                this.top = top;
            }

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }
        }
    }
}
