package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.service.FileStorageService;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceUploadCompositResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;

//@RestController
//@RequestMapping("/api")
//@Transactional
public class StorageController {

//    @Autowired
    private FileStorageService service;

//    @PostMapping("/storage/")
//    @ApiOperation("Upload file")
    public ResponseEntity<RecognitionServiceUploadCompositResponse> handleFileUpload(
        @RequestParam("file") MultipartFile file,
        @RequestParam(value = "categoryId", required = false) String categoryId
    ) throws IOException {

        CompletableFuture<BinaryFile> futureFile = service.storeComposite(file, categoryId);

        return service.handleFileUpload(futureFile.join());
    }

}
