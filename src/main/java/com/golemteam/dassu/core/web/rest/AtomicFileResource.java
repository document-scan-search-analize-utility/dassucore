package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.AtomicFile;
import com.golemteam.dassu.core.repository.AtomicFileRepository;
import com.golemteam.dassu.core.repository.search.AtomicFileSearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.AtomicFile}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class AtomicFileResource {

    private final Logger log = LoggerFactory.getLogger(AtomicFileResource.class);

    private static final String ENTITY_NAME = "dassuCoreAtomicFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AtomicFileRepository atomicFileRepository;

    private final AtomicFileSearchRepository atomicFileSearchRepository;

    public AtomicFileResource(AtomicFileRepository atomicFileRepository, AtomicFileSearchRepository atomicFileSearchRepository) {
        this.atomicFileRepository = atomicFileRepository;
        this.atomicFileSearchRepository = atomicFileSearchRepository;
    }

    /**
     * {@code POST  /atomic-files} : Create a new atomicFile.
     *
     * @param atomicFile the atomicFile to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new atomicFile, or with status {@code 400 (Bad Request)} if the atomicFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/atomic-files")
    public ResponseEntity<AtomicFile> createAtomicFile(@RequestBody AtomicFile atomicFile) throws URISyntaxException {
        log.debug("REST request to save AtomicFile : {}", atomicFile);
        if (atomicFile.getId() != null) {
            throw new BadRequestAlertException("A new atomicFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AtomicFile result = atomicFileRepository.save(atomicFile);
        atomicFileSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/atomic-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /atomic-files} : Updates an existing atomicFile.
     *
     * @param atomicFile the atomicFile to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated atomicFile,
     * or with status {@code 400 (Bad Request)} if the atomicFile is not valid,
     * or with status {@code 500 (Internal Server Error)} if the atomicFile couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/atomic-files")
    public ResponseEntity<AtomicFile> updateAtomicFile(@RequestBody AtomicFile atomicFile) throws URISyntaxException {
        log.debug("REST request to update AtomicFile : {}", atomicFile);
        if (atomicFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AtomicFile result = atomicFileRepository.save(atomicFile);
        atomicFileSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, atomicFile.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /atomic-files} : get all the atomicFiles.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of atomicFiles in body.
     */
    @GetMapping("/atomic-files")
    public List<AtomicFile> getAllAtomicFiles() {
        log.debug("REST request to get all AtomicFiles");
        return atomicFileRepository.findAll();
    }

    /**
     * {@code GET  /atomic-files/:id} : get the "id" atomicFile.
     *
     * @param id the id of the atomicFile to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the atomicFile, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/atomic-files/{id}")
    public ResponseEntity<AtomicFile> getAtomicFile(@PathVariable Long id) {
        log.debug("REST request to get AtomicFile : {}", id);
        Optional<AtomicFile> atomicFile = atomicFileRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(atomicFile);
    }

    /**
     * {@code DELETE  /atomic-files/:id} : delete the "id" atomicFile.
     *
     * @param id the id of the atomicFile to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/atomic-files/{id}")
    public ResponseEntity<Void> deleteAtomicFile(@PathVariable Long id) {
        log.debug("REST request to delete AtomicFile : {}", id);
        atomicFileRepository.deleteById(id);
        atomicFileSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/atomic-files?query=:query} : search for the atomicFile corresponding
     * to the query.
     *
     * @param query the query of the atomicFile search.
     * @return the result of the search.
     */
    @GetMapping("/_search/atomic-files")
    public List<AtomicFile> searchAtomicFiles(@RequestParam String query) {
        log.debug("REST request to search AtomicFiles for query {}", query);
        return StreamSupport
            .stream(atomicFileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
