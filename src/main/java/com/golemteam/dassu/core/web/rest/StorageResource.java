package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.repository.CompositFileRepository;
import com.golemteam.dassu.core.service.FileStorageService;
import com.golemteam.dassu.core.service.RecognitionServiceRequester;
import com.golemteam.dassu.core.web.rest.response.RecognitionServiceUploadCompositResponse;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

/**
 * StorageResource controller
 */
@RestController
@RequestMapping("/api/zuul/storage")
public class StorageResource {

    private final Logger log = LoggerFactory.getLogger(StorageResource.class);

    private final FileStorageService service;
    private final RecognitionServiceRequester serviceRequester;
    private final CompositFileRepository compositFileRepository;

    public StorageResource(FileStorageService service,
                           RecognitionServiceRequester serviceRequester,
                           CompositFileRepository compositFileRepository
    ) {
        this.service = service;
        this.serviceRequester = serviceRequester;
        this.compositFileRepository = compositFileRepository;
    }

    @PostMapping(
        path = "/"
        //, consumes = { "multipart/form-data" }
    )
    @ApiOperation("Upload file")
    public ResponseEntity<RecognitionServiceUploadCompositResponse> handleFileUpload(
        @RequestParam("file") MultipartFile file,
        @RequestParam(value = "categoryId", required = false) String categoryId
    ) throws IOException {

        CompletableFuture<BinaryFile> futureFile = service.storeComposite(file, categoryId);

        return service.handleFileUpload(futureFile.join());
    }

    @GetMapping("/")
    public void testtesttest() throws IOException {
        serviceRequester.documentPredictionRequester();
    }

    @GetMapping("/{compositFileId}/{atomicFileId}")
    public ResponseEntity<Resource> getPngAsResource(
        @PathVariable String compositFileId,
        @PathVariable String atomicFileId) {
        BinaryFile binaryFile = compositFileRepository.getOne(Long.parseLong(compositFileId))
            .getAtomics()
            .stream().filter(atomic -> atomic.getId().toString().equals(atomicFileId))
            .findFirst().get()
            .getBinaryFile();
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
            "attachment; filename=\"" + binaryFile.getName() + "\"")
            .body(service.loadAsResource(binaryFile.getStorageFilePath())
            );
    }


}
