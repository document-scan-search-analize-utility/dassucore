package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.domain.CompositFile;
import com.golemteam.dassu.core.repository.CompositFileRepository;
import com.golemteam.dassu.core.repository.search.CompositFileSearchRepository;
import com.golemteam.dassu.core.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.golemteam.dassu.core.domain.CompositFile}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CompositFileResource {

    private final Logger log = LoggerFactory.getLogger(CompositFileResource.class);

    private static final String ENTITY_NAME = "dassuCoreCompositFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CompositFileRepository compositFileRepository;

    private final CompositFileSearchRepository compositFileSearchRepository;

    public CompositFileResource(CompositFileRepository compositFileRepository, CompositFileSearchRepository compositFileSearchRepository) {
        this.compositFileRepository = compositFileRepository;
        this.compositFileSearchRepository = compositFileSearchRepository;
    }

    /**
     * {@code POST  /composit-files} : Create a new compositFile.
     *
     * @param compositFile the compositFile to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new compositFile, or with status {@code 400 (Bad Request)} if the compositFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/composit-files")
    public ResponseEntity<CompositFile> createCompositFile(@RequestBody CompositFile compositFile) throws URISyntaxException {
        log.debug("REST request to save CompositFile : {}", compositFile);
        if (compositFile.getId() != null) {
            throw new BadRequestAlertException("A new compositFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CompositFile result = compositFileRepository.save(compositFile);
        compositFileSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/composit-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /composit-files} : Updates an existing compositFile.
     *
     * @param compositFile the compositFile to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated compositFile,
     * or with status {@code 400 (Bad Request)} if the compositFile is not valid,
     * or with status {@code 500 (Internal Server Error)} if the compositFile couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/composit-files")
    public ResponseEntity<CompositFile> updateCompositFile(@RequestBody CompositFile compositFile) throws URISyntaxException {
        log.debug("REST request to update CompositFile : {}", compositFile);
        if (compositFile.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CompositFile result = compositFileRepository.save(compositFile);
        compositFileSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, compositFile.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /composit-files} : get all the compositFiles.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of compositFiles in body.
     */
    @GetMapping("/composit-files")
    public List<CompositFile> getAllCompositFiles(@RequestParam(required = false) String filter) {
        if ("filecategoryfieldvalue-is-null".equals(filter)) {
            log.debug("REST request to get all CompositFiles where fileCategoryFieldValue is null");
            return StreamSupport
                .stream(compositFileRepository.findAll().spliterator(), false)
                .filter(compositFile -> compositFile.getFileCategoryFieldValue() == null)
                .collect(Collectors.toList());
        }
        log.debug("REST request to get all CompositFiles");
        return compositFileRepository.findAll();
    }

    /**
     * {@code GET  /composit-files/:id} : get the "id" compositFile.
     *
     * @param id the id of the compositFile to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the compositFile, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/composit-files/{id}")
    public ResponseEntity<CompositFile> getCompositFile(@PathVariable Long id) {
        log.debug("REST request to get CompositFile : {}", id);
        Optional<CompositFile> compositFile = compositFileRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(compositFile);
    }

    /**
     * {@code DELETE  /composit-files/:id} : delete the "id" compositFile.
     *
     * @param id the id of the compositFile to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/composit-files/{id}")
    public ResponseEntity<Void> deleteCompositFile(@PathVariable Long id) {
        log.debug("REST request to delete CompositFile : {}", id);
        compositFileRepository.deleteById(id);
        compositFileSearchRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/composit-files?query=:query} : search for the compositFile corresponding
     * to the query.
     *
     * @param query the query of the compositFile search.
     * @return the result of the search.
     */
    @GetMapping("/_search/composit-files")
    public List<CompositFile> searchCompositFiles(@RequestParam String query) {
        log.debug("REST request to search CompositFiles for query {}", query);
        return StreamSupport
            .stream(compositFileSearchRepository.search(queryStringQuery(query)).spliterator(), false)
        .collect(Collectors.toList());
    }
}
