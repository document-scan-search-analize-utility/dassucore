package com.golemteam.dassu.core.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link AtomicFileSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class AtomicFileSearchRepositoryMockConfiguration {

    @MockBean
    private AtomicFileSearchRepository mockAtomicFileSearchRepository;

}
