package com.golemteam.dassu.core.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link CompositFileSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class CompositFileSearchRepositoryMockConfiguration {

    @MockBean
    private CompositFileSearchRepository mockCompositFileSearchRepository;

}
