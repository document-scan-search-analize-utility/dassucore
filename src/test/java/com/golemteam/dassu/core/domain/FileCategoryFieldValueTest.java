package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class FileCategoryFieldValueTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FileCategoryFieldValue.class);
        FileCategoryFieldValue fileCategoryFieldValue1 = new FileCategoryFieldValue();
        fileCategoryFieldValue1.setId(1L);
        FileCategoryFieldValue fileCategoryFieldValue2 = new FileCategoryFieldValue();
        fileCategoryFieldValue2.setId(fileCategoryFieldValue1.getId());
        assertThat(fileCategoryFieldValue1).isEqualTo(fileCategoryFieldValue2);
        fileCategoryFieldValue2.setId(2L);
        assertThat(fileCategoryFieldValue1).isNotEqualTo(fileCategoryFieldValue2);
        fileCategoryFieldValue1.setId(null);
        assertThat(fileCategoryFieldValue1).isNotEqualTo(fileCategoryFieldValue2);
    }
}
