package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class FileCategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FileCategory.class);
        FileCategory fileCategory1 = new FileCategory();
        fileCategory1.setId(1L);
        FileCategory fileCategory2 = new FileCategory();
        fileCategory2.setId(fileCategory1.getId());
        assertThat(fileCategory1).isEqualTo(fileCategory2);
        fileCategory2.setId(2L);
        assertThat(fileCategory1).isNotEqualTo(fileCategory2);
        fileCategory1.setId(null);
        assertThat(fileCategory1).isNotEqualTo(fileCategory2);
    }
}
