package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class FileCategoryFieldTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FileCategoryField.class);
        FileCategoryField fileCategoryField1 = new FileCategoryField();
        fileCategoryField1.setId(1L);
        FileCategoryField fileCategoryField2 = new FileCategoryField();
        fileCategoryField2.setId(fileCategoryField1.getId());
        assertThat(fileCategoryField1).isEqualTo(fileCategoryField2);
        fileCategoryField2.setId(2L);
        assertThat(fileCategoryField1).isNotEqualTo(fileCategoryField2);
        fileCategoryField1.setId(null);
        assertThat(fileCategoryField1).isNotEqualTo(fileCategoryField2);
    }
}
