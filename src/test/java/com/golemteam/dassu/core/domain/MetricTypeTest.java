package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class MetricTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MetricType.class);
        MetricType metricType1 = new MetricType();
        metricType1.setId(1L);
        MetricType metricType2 = new MetricType();
        metricType2.setId(metricType1.getId());
        assertThat(metricType1).isEqualTo(metricType2);
        metricType2.setId(2L);
        assertThat(metricType1).isNotEqualTo(metricType2);
        metricType1.setId(null);
        assertThat(metricType1).isNotEqualTo(metricType2);
    }
}
