package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class CompositFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CompositFile.class);
        CompositFile compositFile1 = new CompositFile();
        compositFile1.setId(1L);
        CompositFile compositFile2 = new CompositFile();
        compositFile2.setId(compositFile1.getId());
        assertThat(compositFile1).isEqualTo(compositFile2);
        compositFile2.setId(2L);
        assertThat(compositFile1).isNotEqualTo(compositFile2);
        compositFile1.setId(null);
        assertThat(compositFile1).isNotEqualTo(compositFile2);
    }
}
