package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class BinaryFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BinaryFile.class);
        BinaryFile binaryFile1 = new BinaryFile();
        binaryFile1.setId(1L);
        BinaryFile binaryFile2 = new BinaryFile();
        binaryFile2.setId(binaryFile1.getId());
        assertThat(binaryFile1).isEqualTo(binaryFile2);
        binaryFile2.setId(2L);
        assertThat(binaryFile1).isNotEqualTo(binaryFile2);
        binaryFile1.setId(null);
        assertThat(binaryFile1).isNotEqualTo(binaryFile2);
    }
}
