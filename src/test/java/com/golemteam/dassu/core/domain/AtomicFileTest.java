package com.golemteam.dassu.core.domain;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.golemteam.dassu.core.web.rest.TestUtil;

@Disabled
public class AtomicFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AtomicFile.class);
        AtomicFile atomicFile1 = new AtomicFile();
        atomicFile1.setId(1L);
        AtomicFile atomicFile2 = new AtomicFile();
        atomicFile2.setId(atomicFile1.getId());
        assertThat(atomicFile1).isEqualTo(atomicFile2);
        atomicFile2.setId(2L);
        assertThat(atomicFile1).isNotEqualTo(atomicFile2);
        atomicFile1.setId(null);
        assertThat(atomicFile1).isNotEqualTo(atomicFile2);
    }
}
