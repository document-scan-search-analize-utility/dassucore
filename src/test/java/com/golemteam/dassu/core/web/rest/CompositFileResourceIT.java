package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.CompositFile;
import com.golemteam.dassu.core.repository.CompositFileRepository;
import com.golemteam.dassu.core.repository.search.CompositFileSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.golemteam.dassu.core.domain.enumeration.ProcessingStatus;
/**
 * Integration tests for the {@link CompositFileResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class CompositFileResourceIT {

    private static final ProcessingStatus DEFAULT_STATUS = ProcessingStatus.CREATED;
    private static final ProcessingStatus UPDATED_STATUS = ProcessingStatus.PROCESSING;

    private static final String DEFAULT_PRE_PROCESSING_DATA = "AAAAAAAAAA";
    private static final String UPDATED_PRE_PROCESSING_DATA = "BBBBBBBBBB";

    @Autowired
    private CompositFileRepository compositFileRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.CompositFileSearchRepositoryMockConfiguration
     */
    @Autowired
    private CompositFileSearchRepository mockCompositFileSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCompositFileMockMvc;

    private CompositFile compositFile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompositFile createEntity(EntityManager em) {
        CompositFile compositFile = new CompositFile()
            .status(DEFAULT_STATUS)
            .preProcessingData(DEFAULT_PRE_PROCESSING_DATA);
        return compositFile;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CompositFile createUpdatedEntity(EntityManager em) {
        CompositFile compositFile = new CompositFile()
            .status(UPDATED_STATUS)
            .preProcessingData(UPDATED_PRE_PROCESSING_DATA);
        return compositFile;
    }

    @BeforeEach
    public void initTest() {
        compositFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createCompositFile() throws Exception {
        int databaseSizeBeforeCreate = compositFileRepository.findAll().size();
        // Create the CompositFile
        restCompositFileMockMvc.perform(post("/api/composit-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compositFile)))
            .andExpect(status().isCreated());

        // Validate the CompositFile in the database
        List<CompositFile> compositFileList = compositFileRepository.findAll();
        assertThat(compositFileList).hasSize(databaseSizeBeforeCreate + 1);
        CompositFile testCompositFile = compositFileList.get(compositFileList.size() - 1);
        assertThat(testCompositFile.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testCompositFile.getPreProcessingData()).isEqualTo(DEFAULT_PRE_PROCESSING_DATA);

        // Validate the CompositFile in Elasticsearch
        verify(mockCompositFileSearchRepository, times(1)).save(testCompositFile);
    }

    @Test
    @Transactional
    public void createCompositFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = compositFileRepository.findAll().size();

        // Create the CompositFile with an existing ID
        compositFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCompositFileMockMvc.perform(post("/api/composit-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compositFile)))
            .andExpect(status().isBadRequest());

        // Validate the CompositFile in the database
        List<CompositFile> compositFileList = compositFileRepository.findAll();
        assertThat(compositFileList).hasSize(databaseSizeBeforeCreate);

        // Validate the CompositFile in Elasticsearch
        verify(mockCompositFileSearchRepository, times(0)).save(compositFile);
    }


    @Test
    @Transactional
    public void getAllCompositFiles() throws Exception {
        // Initialize the database
        compositFileRepository.saveAndFlush(compositFile);

        // Get all the compositFileList
        restCompositFileMockMvc.perform(get("/api/composit-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compositFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].preProcessingData").value(hasItem(DEFAULT_PRE_PROCESSING_DATA)));
    }

    @Test
    @Transactional
    public void getCompositFile() throws Exception {
        // Initialize the database
        compositFileRepository.saveAndFlush(compositFile);

        // Get the compositFile
        restCompositFileMockMvc.perform(get("/api/composit-files/{id}", compositFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(compositFile.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.preProcessingData").value(DEFAULT_PRE_PROCESSING_DATA));
    }
    @Test
    @Transactional
    public void getNonExistingCompositFile() throws Exception {
        // Get the compositFile
        restCompositFileMockMvc.perform(get("/api/composit-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCompositFile() throws Exception {
        // Initialize the database
        compositFileRepository.saveAndFlush(compositFile);

        int databaseSizeBeforeUpdate = compositFileRepository.findAll().size();

        // Update the compositFile
        CompositFile updatedCompositFile = compositFileRepository.findById(compositFile.getId()).get();
        // Disconnect from session so that the updates on updatedCompositFile are not directly saved in db
        em.detach(updatedCompositFile);
        updatedCompositFile
            .status(UPDATED_STATUS)
            .preProcessingData(UPDATED_PRE_PROCESSING_DATA);

        restCompositFileMockMvc.perform(put("/api/composit-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedCompositFile)))
            .andExpect(status().isOk());

        // Validate the CompositFile in the database
        List<CompositFile> compositFileList = compositFileRepository.findAll();
        assertThat(compositFileList).hasSize(databaseSizeBeforeUpdate);
        CompositFile testCompositFile = compositFileList.get(compositFileList.size() - 1);
        assertThat(testCompositFile.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testCompositFile.getPreProcessingData()).isEqualTo(UPDATED_PRE_PROCESSING_DATA);

        // Validate the CompositFile in Elasticsearch
        verify(mockCompositFileSearchRepository, times(1)).save(testCompositFile);
    }

    @Test
    @Transactional
    public void updateNonExistingCompositFile() throws Exception {
        int databaseSizeBeforeUpdate = compositFileRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCompositFileMockMvc.perform(put("/api/composit-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(compositFile)))
            .andExpect(status().isBadRequest());

        // Validate the CompositFile in the database
        List<CompositFile> compositFileList = compositFileRepository.findAll();
        assertThat(compositFileList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CompositFile in Elasticsearch
        verify(mockCompositFileSearchRepository, times(0)).save(compositFile);
    }

    @Test
    @Transactional
    public void deleteCompositFile() throws Exception {
        // Initialize the database
        compositFileRepository.saveAndFlush(compositFile);

        int databaseSizeBeforeDelete = compositFileRepository.findAll().size();

        // Delete the compositFile
        restCompositFileMockMvc.perform(delete("/api/composit-files/{id}", compositFile.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CompositFile> compositFileList = compositFileRepository.findAll();
        assertThat(compositFileList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CompositFile in Elasticsearch
        verify(mockCompositFileSearchRepository, times(1)).deleteById(compositFile.getId());
    }

    @Test
    @Transactional
    public void searchCompositFile() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        compositFileRepository.saveAndFlush(compositFile);
        when(mockCompositFileSearchRepository.search(queryStringQuery("id:" + compositFile.getId())))
            .thenReturn(Collections.singletonList(compositFile));

        // Search the compositFile
        restCompositFileMockMvc.perform(get("/api/_search/composit-files?query=id:" + compositFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(compositFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].preProcessingData").value(hasItem(DEFAULT_PRE_PROCESSING_DATA)));
    }
}
