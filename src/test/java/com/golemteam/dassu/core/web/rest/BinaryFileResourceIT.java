package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.BinaryFile;
import com.golemteam.dassu.core.repository.BinaryFileRepository;
import com.golemteam.dassu.core.repository.search.BinaryFileSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.golemteam.dassu.core.domain.enumeration.StorageType;
/**
 * Integration tests for the {@link BinaryFileResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class BinaryFileResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String DEFAULT_STORAGE_FILE_PATH = "AAAAAAAAAA";
    private static final String UPDATED_STORAGE_FILE_PATH = "BBBBBBBBBB";

    private static final String DEFAULT_MD_5 = "AAAAAAAAAA";
    private static final String UPDATED_MD_5 = "BBBBBBBBBB";

    private static final StorageType DEFAULT_STORAGE_TYPE = StorageType.LOCAL;
    private static final StorageType UPDATED_STORAGE_TYPE = StorageType.LOCAL;

    @Autowired
    private BinaryFileRepository binaryFileRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.BinaryFileSearchRepositoryMockConfiguration
     */
    @Autowired
    private BinaryFileSearchRepository mockBinaryFileSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBinaryFileMockMvc;

    private BinaryFile binaryFile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BinaryFile createEntity(EntityManager em) {
        BinaryFile binaryFile = new BinaryFile()
            .name(DEFAULT_NAME)
            .version(DEFAULT_VERSION)
            .storageFilePath(DEFAULT_STORAGE_FILE_PATH)
            .md5(DEFAULT_MD_5)
            .storageType(DEFAULT_STORAGE_TYPE);
        return binaryFile;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BinaryFile createUpdatedEntity(EntityManager em) {
        BinaryFile binaryFile = new BinaryFile()
            .name(UPDATED_NAME)
            .version(UPDATED_VERSION)
            .storageFilePath(UPDATED_STORAGE_FILE_PATH)
            .md5(UPDATED_MD_5)
            .storageType(UPDATED_STORAGE_TYPE);
        return binaryFile;
    }

    @BeforeEach
    public void initTest() {
        binaryFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createBinaryFile() throws Exception {
        int databaseSizeBeforeCreate = binaryFileRepository.findAll().size();
        // Create the BinaryFile
        restBinaryFileMockMvc.perform(post("/api/binary-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(binaryFile)))
            .andExpect(status().isCreated());

        // Validate the BinaryFile in the database
        List<BinaryFile> binaryFileList = binaryFileRepository.findAll();
        assertThat(binaryFileList).hasSize(databaseSizeBeforeCreate + 1);
        BinaryFile testBinaryFile = binaryFileList.get(binaryFileList.size() - 1);
        assertThat(testBinaryFile.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testBinaryFile.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testBinaryFile.getStorageFilePath()).isEqualTo(DEFAULT_STORAGE_FILE_PATH);
        assertThat(testBinaryFile.getMd5()).isEqualTo(DEFAULT_MD_5);
        assertThat(testBinaryFile.getStorageType()).isEqualTo(DEFAULT_STORAGE_TYPE);

        // Validate the BinaryFile in Elasticsearch
        verify(mockBinaryFileSearchRepository, times(1)).save(testBinaryFile);
    }

    @Test
    @Transactional
    public void createBinaryFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = binaryFileRepository.findAll().size();

        // Create the BinaryFile with an existing ID
        binaryFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBinaryFileMockMvc.perform(post("/api/binary-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(binaryFile)))
            .andExpect(status().isBadRequest());

        // Validate the BinaryFile in the database
        List<BinaryFile> binaryFileList = binaryFileRepository.findAll();
        assertThat(binaryFileList).hasSize(databaseSizeBeforeCreate);

        // Validate the BinaryFile in Elasticsearch
        verify(mockBinaryFileSearchRepository, times(0)).save(binaryFile);
    }


    @Test
    @Transactional
    public void getAllBinaryFiles() throws Exception {
        // Initialize the database
        binaryFileRepository.saveAndFlush(binaryFile);

        // Get all the binaryFileList
        restBinaryFileMockMvc.perform(get("/api/binary-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(binaryFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].storageFilePath").value(hasItem(DEFAULT_STORAGE_FILE_PATH)))
            .andExpect(jsonPath("$.[*].md5").value(hasItem(DEFAULT_MD_5)))
            .andExpect(jsonPath("$.[*].storageType").value(hasItem(DEFAULT_STORAGE_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getBinaryFile() throws Exception {
        // Initialize the database
        binaryFileRepository.saveAndFlush(binaryFile);

        // Get the binaryFile
        restBinaryFileMockMvc.perform(get("/api/binary-files/{id}", binaryFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(binaryFile.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()))
            .andExpect(jsonPath("$.storageFilePath").value(DEFAULT_STORAGE_FILE_PATH))
            .andExpect(jsonPath("$.md5").value(DEFAULT_MD_5))
            .andExpect(jsonPath("$.storageType").value(DEFAULT_STORAGE_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingBinaryFile() throws Exception {
        // Get the binaryFile
        restBinaryFileMockMvc.perform(get("/api/binary-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBinaryFile() throws Exception {
        // Initialize the database
        binaryFileRepository.saveAndFlush(binaryFile);

        int databaseSizeBeforeUpdate = binaryFileRepository.findAll().size();

        // Update the binaryFile
        BinaryFile updatedBinaryFile = binaryFileRepository.findById(binaryFile.getId()).get();
        // Disconnect from session so that the updates on updatedBinaryFile are not directly saved in db
        em.detach(updatedBinaryFile);
        updatedBinaryFile
            .name(UPDATED_NAME)
            .version(UPDATED_VERSION)
            .storageFilePath(UPDATED_STORAGE_FILE_PATH)
            .md5(UPDATED_MD_5)
            .storageType(UPDATED_STORAGE_TYPE);

        restBinaryFileMockMvc.perform(put("/api/binary-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedBinaryFile)))
            .andExpect(status().isOk());

        // Validate the BinaryFile in the database
        List<BinaryFile> binaryFileList = binaryFileRepository.findAll();
        assertThat(binaryFileList).hasSize(databaseSizeBeforeUpdate);
        BinaryFile testBinaryFile = binaryFileList.get(binaryFileList.size() - 1);
        assertThat(testBinaryFile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testBinaryFile.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testBinaryFile.getStorageFilePath()).isEqualTo(UPDATED_STORAGE_FILE_PATH);
        assertThat(testBinaryFile.getMd5()).isEqualTo(UPDATED_MD_5);
        assertThat(testBinaryFile.getStorageType()).isEqualTo(UPDATED_STORAGE_TYPE);

        // Validate the BinaryFile in Elasticsearch
        verify(mockBinaryFileSearchRepository, times(1)).save(testBinaryFile);
    }

    @Test
    @Transactional
    public void updateNonExistingBinaryFile() throws Exception {
        int databaseSizeBeforeUpdate = binaryFileRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBinaryFileMockMvc.perform(put("/api/binary-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(binaryFile)))
            .andExpect(status().isBadRequest());

        // Validate the BinaryFile in the database
        List<BinaryFile> binaryFileList = binaryFileRepository.findAll();
        assertThat(binaryFileList).hasSize(databaseSizeBeforeUpdate);

        // Validate the BinaryFile in Elasticsearch
        verify(mockBinaryFileSearchRepository, times(0)).save(binaryFile);
    }

    @Test
    @Transactional
    public void deleteBinaryFile() throws Exception {
        // Initialize the database
        binaryFileRepository.saveAndFlush(binaryFile);

        int databaseSizeBeforeDelete = binaryFileRepository.findAll().size();

        // Delete the binaryFile
        restBinaryFileMockMvc.perform(delete("/api/binary-files/{id}", binaryFile.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BinaryFile> binaryFileList = binaryFileRepository.findAll();
        assertThat(binaryFileList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the BinaryFile in Elasticsearch
        verify(mockBinaryFileSearchRepository, times(1)).deleteById(binaryFile.getId());
    }

    @Test
    @Transactional
    public void searchBinaryFile() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        binaryFileRepository.saveAndFlush(binaryFile);
        when(mockBinaryFileSearchRepository.search(queryStringQuery("id:" + binaryFile.getId())))
            .thenReturn(Collections.singletonList(binaryFile));

        // Search the binaryFile
        restBinaryFileMockMvc.perform(get("/api/_search/binary-files?query=id:" + binaryFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(binaryFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].storageFilePath").value(hasItem(DEFAULT_STORAGE_FILE_PATH)))
            .andExpect(jsonPath("$.[*].md5").value(hasItem(DEFAULT_MD_5)))
            .andExpect(jsonPath("$.[*].storageType").value(hasItem(DEFAULT_STORAGE_TYPE.toString())));
    }
}
