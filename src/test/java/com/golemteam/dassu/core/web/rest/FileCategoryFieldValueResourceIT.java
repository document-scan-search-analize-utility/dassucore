package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.FileCategoryFieldValue;
import com.golemteam.dassu.core.repository.FileCategoryFieldValueRepository;
import com.golemteam.dassu.core.repository.search.FileCategoryFieldValueSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FileCategoryFieldValueResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class FileCategoryFieldValueResourceIT {

    private static final String DEFAULT_VALUE = "AAAAAAAAAA";
    private static final String UPDATED_VALUE = "BBBBBBBBBB";

    @Autowired
    private FileCategoryFieldValueRepository fileCategoryFieldValueRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.FileCategoryFieldValueSearchRepositoryMockConfiguration
     */
    @Autowired
    private FileCategoryFieldValueSearchRepository mockFileCategoryFieldValueSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFileCategoryFieldValueMockMvc;

    private FileCategoryFieldValue fileCategoryFieldValue;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategoryFieldValue createEntity(EntityManager em) {
        FileCategoryFieldValue fileCategoryFieldValue = new FileCategoryFieldValue()
            .value(DEFAULT_VALUE);
        return fileCategoryFieldValue;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategoryFieldValue createUpdatedEntity(EntityManager em) {
        FileCategoryFieldValue fileCategoryFieldValue = new FileCategoryFieldValue()
            .value(UPDATED_VALUE);
        return fileCategoryFieldValue;
    }

    @BeforeEach
    public void initTest() {
        fileCategoryFieldValue = createEntity(em);
    }

    @Test
    @Transactional
    public void createFileCategoryFieldValue() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryFieldValueRepository.findAll().size();
        // Create the FileCategoryFieldValue
        restFileCategoryFieldValueMockMvc.perform(post("/api/file-category-field-values").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryFieldValue)))
            .andExpect(status().isCreated());

        // Validate the FileCategoryFieldValue in the database
        List<FileCategoryFieldValue> fileCategoryFieldValueList = fileCategoryFieldValueRepository.findAll();
        assertThat(fileCategoryFieldValueList).hasSize(databaseSizeBeforeCreate + 1);
        FileCategoryFieldValue testFileCategoryFieldValue = fileCategoryFieldValueList.get(fileCategoryFieldValueList.size() - 1);
        assertThat(testFileCategoryFieldValue.getValue()).isEqualTo(DEFAULT_VALUE);

        // Validate the FileCategoryFieldValue in Elasticsearch
        verify(mockFileCategoryFieldValueSearchRepository, times(1)).save(testFileCategoryFieldValue);
    }

    @Test
    @Transactional
    public void createFileCategoryFieldValueWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryFieldValueRepository.findAll().size();

        // Create the FileCategoryFieldValue with an existing ID
        fileCategoryFieldValue.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFileCategoryFieldValueMockMvc.perform(post("/api/file-category-field-values").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryFieldValue)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategoryFieldValue in the database
        List<FileCategoryFieldValue> fileCategoryFieldValueList = fileCategoryFieldValueRepository.findAll();
        assertThat(fileCategoryFieldValueList).hasSize(databaseSizeBeforeCreate);

        // Validate the FileCategoryFieldValue in Elasticsearch
        verify(mockFileCategoryFieldValueSearchRepository, times(0)).save(fileCategoryFieldValue);
    }


    @Test
    @Transactional
    public void getAllFileCategoryFieldValues() throws Exception {
        // Initialize the database
        fileCategoryFieldValueRepository.saveAndFlush(fileCategoryFieldValue);

        // Get all the fileCategoryFieldValueList
        restFileCategoryFieldValueMockMvc.perform(get("/api/file-category-field-values?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategoryFieldValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)));
    }

    @Test
    @Transactional
    public void getFileCategoryFieldValue() throws Exception {
        // Initialize the database
        fileCategoryFieldValueRepository.saveAndFlush(fileCategoryFieldValue);

        // Get the fileCategoryFieldValue
        restFileCategoryFieldValueMockMvc.perform(get("/api/file-category-field-values/{id}", fileCategoryFieldValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fileCategoryFieldValue.getId().intValue()))
            .andExpect(jsonPath("$.value").value(DEFAULT_VALUE));
    }
    @Test
    @Transactional
    public void getNonExistingFileCategoryFieldValue() throws Exception {
        // Get the fileCategoryFieldValue
        restFileCategoryFieldValueMockMvc.perform(get("/api/file-category-field-values/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFileCategoryFieldValue() throws Exception {
        // Initialize the database
        fileCategoryFieldValueRepository.saveAndFlush(fileCategoryFieldValue);

        int databaseSizeBeforeUpdate = fileCategoryFieldValueRepository.findAll().size();

        // Update the fileCategoryFieldValue
        FileCategoryFieldValue updatedFileCategoryFieldValue = fileCategoryFieldValueRepository.findById(fileCategoryFieldValue.getId()).get();
        // Disconnect from session so that the updates on updatedFileCategoryFieldValue are not directly saved in db
        em.detach(updatedFileCategoryFieldValue);
        updatedFileCategoryFieldValue
            .value(UPDATED_VALUE);

        restFileCategoryFieldValueMockMvc.perform(put("/api/file-category-field-values").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFileCategoryFieldValue)))
            .andExpect(status().isOk());

        // Validate the FileCategoryFieldValue in the database
        List<FileCategoryFieldValue> fileCategoryFieldValueList = fileCategoryFieldValueRepository.findAll();
        assertThat(fileCategoryFieldValueList).hasSize(databaseSizeBeforeUpdate);
        FileCategoryFieldValue testFileCategoryFieldValue = fileCategoryFieldValueList.get(fileCategoryFieldValueList.size() - 1);
        assertThat(testFileCategoryFieldValue.getValue()).isEqualTo(UPDATED_VALUE);

        // Validate the FileCategoryFieldValue in Elasticsearch
        verify(mockFileCategoryFieldValueSearchRepository, times(1)).save(testFileCategoryFieldValue);
    }

    @Test
    @Transactional
    public void updateNonExistingFileCategoryFieldValue() throws Exception {
        int databaseSizeBeforeUpdate = fileCategoryFieldValueRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFileCategoryFieldValueMockMvc.perform(put("/api/file-category-field-values").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryFieldValue)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategoryFieldValue in the database
        List<FileCategoryFieldValue> fileCategoryFieldValueList = fileCategoryFieldValueRepository.findAll();
        assertThat(fileCategoryFieldValueList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FileCategoryFieldValue in Elasticsearch
        verify(mockFileCategoryFieldValueSearchRepository, times(0)).save(fileCategoryFieldValue);
    }

    @Test
    @Transactional
    public void deleteFileCategoryFieldValue() throws Exception {
        // Initialize the database
        fileCategoryFieldValueRepository.saveAndFlush(fileCategoryFieldValue);

        int databaseSizeBeforeDelete = fileCategoryFieldValueRepository.findAll().size();

        // Delete the fileCategoryFieldValue
        restFileCategoryFieldValueMockMvc.perform(delete("/api/file-category-field-values/{id}", fileCategoryFieldValue.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FileCategoryFieldValue> fileCategoryFieldValueList = fileCategoryFieldValueRepository.findAll();
        assertThat(fileCategoryFieldValueList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FileCategoryFieldValue in Elasticsearch
        verify(mockFileCategoryFieldValueSearchRepository, times(1)).deleteById(fileCategoryFieldValue.getId());
    }

    @Test
    @Transactional
    public void searchFileCategoryFieldValue() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        fileCategoryFieldValueRepository.saveAndFlush(fileCategoryFieldValue);
        when(mockFileCategoryFieldValueSearchRepository.search(queryStringQuery("id:" + fileCategoryFieldValue.getId())))
            .thenReturn(Collections.singletonList(fileCategoryFieldValue));

        // Search the fileCategoryFieldValue
        restFileCategoryFieldValueMockMvc.perform(get("/api/_search/file-category-field-values?query=id:" + fileCategoryFieldValue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategoryFieldValue.getId().intValue())))
            .andExpect(jsonPath("$.[*].value").value(hasItem(DEFAULT_VALUE)));
    }
}
