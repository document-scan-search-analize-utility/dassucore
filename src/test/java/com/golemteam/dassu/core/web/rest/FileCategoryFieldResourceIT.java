package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.FileCategoryField;
import com.golemteam.dassu.core.repository.FileCategoryFieldRepository;
import com.golemteam.dassu.core.repository.search.FileCategoryFieldSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FileCategoryFieldResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class FileCategoryFieldResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_META = "AAAAAAAAAA";
    private static final String UPDATED_META = "BBBBBBBBBB";

    @Autowired
    private FileCategoryFieldRepository fileCategoryFieldRepository;

    @Mock
    private FileCategoryFieldRepository fileCategoryFieldRepositoryMock;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.FileCategoryFieldSearchRepositoryMockConfiguration
     */
    @Autowired
    private FileCategoryFieldSearchRepository mockFileCategoryFieldSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFileCategoryFieldMockMvc;

    private FileCategoryField fileCategoryField;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategoryField createEntity(EntityManager em) {
        FileCategoryField fileCategoryField = new FileCategoryField()
            .name(DEFAULT_NAME)
            .meta(DEFAULT_META);
        return fileCategoryField;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategoryField createUpdatedEntity(EntityManager em) {
        FileCategoryField fileCategoryField = new FileCategoryField()
            .name(UPDATED_NAME)
            .meta(UPDATED_META);
        return fileCategoryField;
    }

    @BeforeEach
    public void initTest() {
        fileCategoryField = createEntity(em);
    }

    @Test
    @Transactional
    public void createFileCategoryField() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryFieldRepository.findAll().size();
        // Create the FileCategoryField
        restFileCategoryFieldMockMvc.perform(post("/api/file-category-fields").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryField)))
            .andExpect(status().isCreated());

        // Validate the FileCategoryField in the database
        List<FileCategoryField> fileCategoryFieldList = fileCategoryFieldRepository.findAll();
        assertThat(fileCategoryFieldList).hasSize(databaseSizeBeforeCreate + 1);
        FileCategoryField testFileCategoryField = fileCategoryFieldList.get(fileCategoryFieldList.size() - 1);
        assertThat(testFileCategoryField.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFileCategoryField.getMeta()).isEqualTo(DEFAULT_META);

        // Validate the FileCategoryField in Elasticsearch
        verify(mockFileCategoryFieldSearchRepository, times(1)).save(testFileCategoryField);
    }

    @Test
    @Transactional
    public void createFileCategoryFieldWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryFieldRepository.findAll().size();

        // Create the FileCategoryField with an existing ID
        fileCategoryField.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFileCategoryFieldMockMvc.perform(post("/api/file-category-fields").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryField)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategoryField in the database
        List<FileCategoryField> fileCategoryFieldList = fileCategoryFieldRepository.findAll();
        assertThat(fileCategoryFieldList).hasSize(databaseSizeBeforeCreate);

        // Validate the FileCategoryField in Elasticsearch
        verify(mockFileCategoryFieldSearchRepository, times(0)).save(fileCategoryField);
    }


    @Test
    @Transactional
    public void getAllFileCategoryFields() throws Exception {
        // Initialize the database
        fileCategoryFieldRepository.saveAndFlush(fileCategoryField);

        // Get all the fileCategoryFieldList
        restFileCategoryFieldMockMvc.perform(get("/api/file-category-fields?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategoryField.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].meta").value(hasItem(DEFAULT_META)));
    }

    @SuppressWarnings({"unchecked"})
    public void getAllFileCategoryFieldsWithEagerRelationshipsIsEnabled() throws Exception {
        when(fileCategoryFieldRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restFileCategoryFieldMockMvc.perform(get("/api/file-category-fields?eagerload=true"))
            .andExpect(status().isOk());

        verify(fileCategoryFieldRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllFileCategoryFieldsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(fileCategoryFieldRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restFileCategoryFieldMockMvc.perform(get("/api/file-category-fields?eagerload=true"))
            .andExpect(status().isOk());

        verify(fileCategoryFieldRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getFileCategoryField() throws Exception {
        // Initialize the database
        fileCategoryFieldRepository.saveAndFlush(fileCategoryField);

        // Get the fileCategoryField
        restFileCategoryFieldMockMvc.perform(get("/api/file-category-fields/{id}", fileCategoryField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fileCategoryField.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.meta").value(DEFAULT_META));
    }
    @Test
    @Transactional
    public void getNonExistingFileCategoryField() throws Exception {
        // Get the fileCategoryField
        restFileCategoryFieldMockMvc.perform(get("/api/file-category-fields/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFileCategoryField() throws Exception {
        // Initialize the database
        fileCategoryFieldRepository.saveAndFlush(fileCategoryField);

        int databaseSizeBeforeUpdate = fileCategoryFieldRepository.findAll().size();

        // Update the fileCategoryField
        FileCategoryField updatedFileCategoryField = fileCategoryFieldRepository.findById(fileCategoryField.getId()).get();
        // Disconnect from session so that the updates on updatedFileCategoryField are not directly saved in db
        em.detach(updatedFileCategoryField);
        updatedFileCategoryField
            .name(UPDATED_NAME)
            .meta(UPDATED_META);

        restFileCategoryFieldMockMvc.perform(put("/api/file-category-fields").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFileCategoryField)))
            .andExpect(status().isOk());

        // Validate the FileCategoryField in the database
        List<FileCategoryField> fileCategoryFieldList = fileCategoryFieldRepository.findAll();
        assertThat(fileCategoryFieldList).hasSize(databaseSizeBeforeUpdate);
        FileCategoryField testFileCategoryField = fileCategoryFieldList.get(fileCategoryFieldList.size() - 1);
        assertThat(testFileCategoryField.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFileCategoryField.getMeta()).isEqualTo(UPDATED_META);

        // Validate the FileCategoryField in Elasticsearch
        verify(mockFileCategoryFieldSearchRepository, times(1)).save(testFileCategoryField);
    }

    @Test
    @Transactional
    public void updateNonExistingFileCategoryField() throws Exception {
        int databaseSizeBeforeUpdate = fileCategoryFieldRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFileCategoryFieldMockMvc.perform(put("/api/file-category-fields").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategoryField)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategoryField in the database
        List<FileCategoryField> fileCategoryFieldList = fileCategoryFieldRepository.findAll();
        assertThat(fileCategoryFieldList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FileCategoryField in Elasticsearch
        verify(mockFileCategoryFieldSearchRepository, times(0)).save(fileCategoryField);
    }

    @Test
    @Transactional
    public void deleteFileCategoryField() throws Exception {
        // Initialize the database
        fileCategoryFieldRepository.saveAndFlush(fileCategoryField);

        int databaseSizeBeforeDelete = fileCategoryFieldRepository.findAll().size();

        // Delete the fileCategoryField
        restFileCategoryFieldMockMvc.perform(delete("/api/file-category-fields/{id}", fileCategoryField.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FileCategoryField> fileCategoryFieldList = fileCategoryFieldRepository.findAll();
        assertThat(fileCategoryFieldList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FileCategoryField in Elasticsearch
        verify(mockFileCategoryFieldSearchRepository, times(1)).deleteById(fileCategoryField.getId());
    }

    @Test
    @Transactional
    public void searchFileCategoryField() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        fileCategoryFieldRepository.saveAndFlush(fileCategoryField);
        when(mockFileCategoryFieldSearchRepository.search(queryStringQuery("id:" + fileCategoryField.getId())))
            .thenReturn(Collections.singletonList(fileCategoryField));

        // Search the fileCategoryField
        restFileCategoryFieldMockMvc.perform(get("/api/_search/file-category-fields?query=id:" + fileCategoryField.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategoryField.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].meta").value(hasItem(DEFAULT_META)));
    }
}
