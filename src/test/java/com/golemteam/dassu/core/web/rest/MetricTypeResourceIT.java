package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.MetricType;
import com.golemteam.dassu.core.repository.MetricTypeRepository;
import com.golemteam.dassu.core.repository.search.MetricTypeSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MetricTypeResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class MetricTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MetricTypeRepository metricTypeRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.MetricTypeSearchRepositoryMockConfiguration
     */
    @Autowired
    private MetricTypeSearchRepository mockMetricTypeSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMetricTypeMockMvc;

    private MetricType metricType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MetricType createEntity(EntityManager em) {
        MetricType metricType = new MetricType()
            .name(DEFAULT_NAME);
        return metricType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MetricType createUpdatedEntity(EntityManager em) {
        MetricType metricType = new MetricType()
            .name(UPDATED_NAME);
        return metricType;
    }

    @BeforeEach
    public void initTest() {
        metricType = createEntity(em);
    }

    @Test
    @Transactional
    public void createMetricType() throws Exception {
        int databaseSizeBeforeCreate = metricTypeRepository.findAll().size();
        // Create the MetricType
        restMetricTypeMockMvc.perform(post("/api/metric-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(metricType)))
            .andExpect(status().isCreated());

        // Validate the MetricType in the database
        List<MetricType> metricTypeList = metricTypeRepository.findAll();
        assertThat(metricTypeList).hasSize(databaseSizeBeforeCreate + 1);
        MetricType testMetricType = metricTypeList.get(metricTypeList.size() - 1);
        assertThat(testMetricType.getName()).isEqualTo(DEFAULT_NAME);

        // Validate the MetricType in Elasticsearch
        verify(mockMetricTypeSearchRepository, times(1)).save(testMetricType);
    }

    @Test
    @Transactional
    public void createMetricTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = metricTypeRepository.findAll().size();

        // Create the MetricType with an existing ID
        metricType.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMetricTypeMockMvc.perform(post("/api/metric-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(metricType)))
            .andExpect(status().isBadRequest());

        // Validate the MetricType in the database
        List<MetricType> metricTypeList = metricTypeRepository.findAll();
        assertThat(metricTypeList).hasSize(databaseSizeBeforeCreate);

        // Validate the MetricType in Elasticsearch
        verify(mockMetricTypeSearchRepository, times(0)).save(metricType);
    }


    @Test
    @Transactional
    public void getAllMetricTypes() throws Exception {
        // Initialize the database
        metricTypeRepository.saveAndFlush(metricType);

        // Get all the metricTypeList
        restMetricTypeMockMvc.perform(get("/api/metric-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metricType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    public void getMetricType() throws Exception {
        // Initialize the database
        metricTypeRepository.saveAndFlush(metricType);

        // Get the metricType
        restMetricTypeMockMvc.perform(get("/api/metric-types/{id}", metricType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(metricType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingMetricType() throws Exception {
        // Get the metricType
        restMetricTypeMockMvc.perform(get("/api/metric-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMetricType() throws Exception {
        // Initialize the database
        metricTypeRepository.saveAndFlush(metricType);

        int databaseSizeBeforeUpdate = metricTypeRepository.findAll().size();

        // Update the metricType
        MetricType updatedMetricType = metricTypeRepository.findById(metricType.getId()).get();
        // Disconnect from session so that the updates on updatedMetricType are not directly saved in db
        em.detach(updatedMetricType);
        updatedMetricType
            .name(UPDATED_NAME);

        restMetricTypeMockMvc.perform(put("/api/metric-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedMetricType)))
            .andExpect(status().isOk());

        // Validate the MetricType in the database
        List<MetricType> metricTypeList = metricTypeRepository.findAll();
        assertThat(metricTypeList).hasSize(databaseSizeBeforeUpdate);
        MetricType testMetricType = metricTypeList.get(metricTypeList.size() - 1);
        assertThat(testMetricType.getName()).isEqualTo(UPDATED_NAME);

        // Validate the MetricType in Elasticsearch
        verify(mockMetricTypeSearchRepository, times(1)).save(testMetricType);
    }

    @Test
    @Transactional
    public void updateNonExistingMetricType() throws Exception {
        int databaseSizeBeforeUpdate = metricTypeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMetricTypeMockMvc.perform(put("/api/metric-types").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(metricType)))
            .andExpect(status().isBadRequest());

        // Validate the MetricType in the database
        List<MetricType> metricTypeList = metricTypeRepository.findAll();
        assertThat(metricTypeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the MetricType in Elasticsearch
        verify(mockMetricTypeSearchRepository, times(0)).save(metricType);
    }

    @Test
    @Transactional
    public void deleteMetricType() throws Exception {
        // Initialize the database
        metricTypeRepository.saveAndFlush(metricType);

        int databaseSizeBeforeDelete = metricTypeRepository.findAll().size();

        // Delete the metricType
        restMetricTypeMockMvc.perform(delete("/api/metric-types/{id}", metricType.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MetricType> metricTypeList = metricTypeRepository.findAll();
        assertThat(metricTypeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the MetricType in Elasticsearch
        verify(mockMetricTypeSearchRepository, times(1)).deleteById(metricType.getId());
    }

    @Test
    @Transactional
    public void searchMetricType() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        metricTypeRepository.saveAndFlush(metricType);
        when(mockMetricTypeSearchRepository.search(queryStringQuery("id:" + metricType.getId())))
            .thenReturn(Collections.singletonList(metricType));

        // Search the metricType
        restMetricTypeMockMvc.perform(get("/api/_search/metric-types?query=id:" + metricType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(metricType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
}
