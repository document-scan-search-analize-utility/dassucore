package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.FileCategory;
import com.golemteam.dassu.core.repository.FileCategoryRepository;
import com.golemteam.dassu.core.repository.search.FileCategorySearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FileCategoryResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class FileCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private FileCategoryRepository fileCategoryRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.FileCategorySearchRepositoryMockConfiguration
     */
    @Autowired
    private FileCategorySearchRepository mockFileCategorySearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFileCategoryMockMvc;

    private FileCategory fileCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategory createEntity(EntityManager em) {
        FileCategory fileCategory = new FileCategory()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return fileCategory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FileCategory createUpdatedEntity(EntityManager em) {
        FileCategory fileCategory = new FileCategory()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return fileCategory;
    }

    @BeforeEach
    public void initTest() {
        fileCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createFileCategory() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryRepository.findAll().size();
        // Create the FileCategory
        restFileCategoryMockMvc.perform(post("/api/file-categories").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategory)))
            .andExpect(status().isCreated());

        // Validate the FileCategory in the database
        List<FileCategory> fileCategoryList = fileCategoryRepository.findAll();
        assertThat(fileCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        FileCategory testFileCategory = fileCategoryList.get(fileCategoryList.size() - 1);
        assertThat(testFileCategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFileCategory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the FileCategory in Elasticsearch
        verify(mockFileCategorySearchRepository, times(1)).save(testFileCategory);
    }

    @Test
    @Transactional
    public void createFileCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fileCategoryRepository.findAll().size();

        // Create the FileCategory with an existing ID
        fileCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFileCategoryMockMvc.perform(post("/api/file-categories").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategory)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategory in the database
        List<FileCategory> fileCategoryList = fileCategoryRepository.findAll();
        assertThat(fileCategoryList).hasSize(databaseSizeBeforeCreate);

        // Validate the FileCategory in Elasticsearch
        verify(mockFileCategorySearchRepository, times(0)).save(fileCategory);
    }


    @Test
    @Transactional
    public void getAllFileCategories() throws Exception {
        // Initialize the database
        fileCategoryRepository.saveAndFlush(fileCategory);

        // Get all the fileCategoryList
        restFileCategoryMockMvc.perform(get("/api/file-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @Test
    @Transactional
    public void getFileCategory() throws Exception {
        // Initialize the database
        fileCategoryRepository.saveAndFlush(fileCategory);

        // Get the fileCategory
        restFileCategoryMockMvc.perform(get("/api/file-categories/{id}", fileCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fileCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingFileCategory() throws Exception {
        // Get the fileCategory
        restFileCategoryMockMvc.perform(get("/api/file-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFileCategory() throws Exception {
        // Initialize the database
        fileCategoryRepository.saveAndFlush(fileCategory);

        int databaseSizeBeforeUpdate = fileCategoryRepository.findAll().size();

        // Update the fileCategory
        FileCategory updatedFileCategory = fileCategoryRepository.findById(fileCategory.getId()).get();
        // Disconnect from session so that the updates on updatedFileCategory are not directly saved in db
        em.detach(updatedFileCategory);
        updatedFileCategory
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restFileCategoryMockMvc.perform(put("/api/file-categories").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedFileCategory)))
            .andExpect(status().isOk());

        // Validate the FileCategory in the database
        List<FileCategory> fileCategoryList = fileCategoryRepository.findAll();
        assertThat(fileCategoryList).hasSize(databaseSizeBeforeUpdate);
        FileCategory testFileCategory = fileCategoryList.get(fileCategoryList.size() - 1);
        assertThat(testFileCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFileCategory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the FileCategory in Elasticsearch
        verify(mockFileCategorySearchRepository, times(1)).save(testFileCategory);
    }

    @Test
    @Transactional
    public void updateNonExistingFileCategory() throws Exception {
        int databaseSizeBeforeUpdate = fileCategoryRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFileCategoryMockMvc.perform(put("/api/file-categories").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fileCategory)))
            .andExpect(status().isBadRequest());

        // Validate the FileCategory in the database
        List<FileCategory> fileCategoryList = fileCategoryRepository.findAll();
        assertThat(fileCategoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the FileCategory in Elasticsearch
        verify(mockFileCategorySearchRepository, times(0)).save(fileCategory);
    }

    @Test
    @Transactional
    public void deleteFileCategory() throws Exception {
        // Initialize the database
        fileCategoryRepository.saveAndFlush(fileCategory);

        int databaseSizeBeforeDelete = fileCategoryRepository.findAll().size();

        // Delete the fileCategory
        restFileCategoryMockMvc.perform(delete("/api/file-categories/{id}", fileCategory.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FileCategory> fileCategoryList = fileCategoryRepository.findAll();
        assertThat(fileCategoryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the FileCategory in Elasticsearch
        verify(mockFileCategorySearchRepository, times(1)).deleteById(fileCategory.getId());
    }

    @Test
    @Transactional
    public void searchFileCategory() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        fileCategoryRepository.saveAndFlush(fileCategory);
        when(mockFileCategorySearchRepository.search(queryStringQuery("id:" + fileCategory.getId())))
            .thenReturn(Collections.singletonList(fileCategory));

        // Search the fileCategory
        restFileCategoryMockMvc.perform(get("/api/_search/file-categories?query=id:" + fileCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fileCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
}
