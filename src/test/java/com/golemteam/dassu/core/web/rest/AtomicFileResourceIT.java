package com.golemteam.dassu.core.web.rest;

import com.golemteam.dassu.core.DassuCoreApp;
import com.golemteam.dassu.core.config.SecurityBeanOverrideConfiguration;
import com.golemteam.dassu.core.domain.AtomicFile;
import com.golemteam.dassu.core.repository.AtomicFileRepository;
import com.golemteam.dassu.core.repository.search.AtomicFileSearchRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AtomicFileResource} REST controller.
 */
@SpringBootTest(classes = { SecurityBeanOverrideConfiguration.class, DassuCoreApp.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
@Disabled
public class AtomicFileResourceIT {

    private static final String DEFAULT_ANNOTATED_DATA = "AAAAAAAAAA";
    private static final String UPDATED_ANNOTATED_DATA = "BBBBBBBBBB";

    private static final String DEFAULT_ANNOTATED_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_ANNOTATED_TEXT = "BBBBBBBBBB";

    @Autowired
    private AtomicFileRepository atomicFileRepository;

    /**
     * This repository is mocked in the com.golemteam.dassu.core.repository.search test package.
     *
     * @see com.golemteam.dassu.core.repository.search.AtomicFileSearchRepositoryMockConfiguration
     */
    @Autowired
    private AtomicFileSearchRepository mockAtomicFileSearchRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAtomicFileMockMvc;

    private AtomicFile atomicFile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtomicFile createEntity(EntityManager em) {
        AtomicFile atomicFile = new AtomicFile()
            .annotatedData(DEFAULT_ANNOTATED_DATA)
            .annotatedText(DEFAULT_ANNOTATED_TEXT);
        return atomicFile;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AtomicFile createUpdatedEntity(EntityManager em) {
        AtomicFile atomicFile = new AtomicFile()
            .annotatedData(UPDATED_ANNOTATED_DATA)
            .annotatedText(UPDATED_ANNOTATED_TEXT);
        return atomicFile;
    }

    @BeforeEach
    public void initTest() {
        atomicFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createAtomicFile() throws Exception {
        int databaseSizeBeforeCreate = atomicFileRepository.findAll().size();
        // Create the AtomicFile
        restAtomicFileMockMvc.perform(post("/api/atomic-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(atomicFile)))
            .andExpect(status().isCreated());

        // Validate the AtomicFile in the database
        List<AtomicFile> atomicFileList = atomicFileRepository.findAll();
        assertThat(atomicFileList).hasSize(databaseSizeBeforeCreate + 1);
        AtomicFile testAtomicFile = atomicFileList.get(atomicFileList.size() - 1);
        assertThat(testAtomicFile.getAnnotatedData()).isEqualTo(DEFAULT_ANNOTATED_DATA);
        assertThat(testAtomicFile.getAnnotatedText()).isEqualTo(DEFAULT_ANNOTATED_TEXT);

        // Validate the AtomicFile in Elasticsearch
        verify(mockAtomicFileSearchRepository, times(1)).save(testAtomicFile);
    }

    @Test
    @Transactional
    public void createAtomicFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = atomicFileRepository.findAll().size();

        // Create the AtomicFile with an existing ID
        atomicFile.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAtomicFileMockMvc.perform(post("/api/atomic-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(atomicFile)))
            .andExpect(status().isBadRequest());

        // Validate the AtomicFile in the database
        List<AtomicFile> atomicFileList = atomicFileRepository.findAll();
        assertThat(atomicFileList).hasSize(databaseSizeBeforeCreate);

        // Validate the AtomicFile in Elasticsearch
        verify(mockAtomicFileSearchRepository, times(0)).save(atomicFile);
    }


    @Test
    @Transactional
    public void getAllAtomicFiles() throws Exception {
        // Initialize the database
        atomicFileRepository.saveAndFlush(atomicFile);

        // Get all the atomicFileList
        restAtomicFileMockMvc.perform(get("/api/atomic-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atomicFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].annotatedData").value(hasItem(DEFAULT_ANNOTATED_DATA)))
            .andExpect(jsonPath("$.[*].annotatedText").value(hasItem(DEFAULT_ANNOTATED_TEXT)));
    }

    @Test
    @Transactional
    public void getAtomicFile() throws Exception {
        // Initialize the database
        atomicFileRepository.saveAndFlush(atomicFile);

        // Get the atomicFile
        restAtomicFileMockMvc.perform(get("/api/atomic-files/{id}", atomicFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(atomicFile.getId().intValue()))
            .andExpect(jsonPath("$.annotatedData").value(DEFAULT_ANNOTATED_DATA))
            .andExpect(jsonPath("$.annotatedText").value(DEFAULT_ANNOTATED_TEXT));
    }
    @Test
    @Transactional
    public void getNonExistingAtomicFile() throws Exception {
        // Get the atomicFile
        restAtomicFileMockMvc.perform(get("/api/atomic-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAtomicFile() throws Exception {
        // Initialize the database
        atomicFileRepository.saveAndFlush(atomicFile);

        int databaseSizeBeforeUpdate = atomicFileRepository.findAll().size();

        // Update the atomicFile
        AtomicFile updatedAtomicFile = atomicFileRepository.findById(atomicFile.getId()).get();
        // Disconnect from session so that the updates on updatedAtomicFile are not directly saved in db
        em.detach(updatedAtomicFile);
        updatedAtomicFile
            .annotatedData(UPDATED_ANNOTATED_DATA)
            .annotatedText(UPDATED_ANNOTATED_TEXT);

        restAtomicFileMockMvc.perform(put("/api/atomic-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedAtomicFile)))
            .andExpect(status().isOk());

        // Validate the AtomicFile in the database
        List<AtomicFile> atomicFileList = atomicFileRepository.findAll();
        assertThat(atomicFileList).hasSize(databaseSizeBeforeUpdate);
        AtomicFile testAtomicFile = atomicFileList.get(atomicFileList.size() - 1);
        assertThat(testAtomicFile.getAnnotatedData()).isEqualTo(UPDATED_ANNOTATED_DATA);
        assertThat(testAtomicFile.getAnnotatedText()).isEqualTo(UPDATED_ANNOTATED_TEXT);

        // Validate the AtomicFile in Elasticsearch
        verify(mockAtomicFileSearchRepository, times(1)).save(testAtomicFile);
    }

    @Test
    @Transactional
    public void updateNonExistingAtomicFile() throws Exception {
        int databaseSizeBeforeUpdate = atomicFileRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAtomicFileMockMvc.perform(put("/api/atomic-files").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(atomicFile)))
            .andExpect(status().isBadRequest());

        // Validate the AtomicFile in the database
        List<AtomicFile> atomicFileList = atomicFileRepository.findAll();
        assertThat(atomicFileList).hasSize(databaseSizeBeforeUpdate);

        // Validate the AtomicFile in Elasticsearch
        verify(mockAtomicFileSearchRepository, times(0)).save(atomicFile);
    }

    @Test
    @Transactional
    public void deleteAtomicFile() throws Exception {
        // Initialize the database
        atomicFileRepository.saveAndFlush(atomicFile);

        int databaseSizeBeforeDelete = atomicFileRepository.findAll().size();

        // Delete the atomicFile
        restAtomicFileMockMvc.perform(delete("/api/atomic-files/{id}", atomicFile.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AtomicFile> atomicFileList = atomicFileRepository.findAll();
        assertThat(atomicFileList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the AtomicFile in Elasticsearch
        verify(mockAtomicFileSearchRepository, times(1)).deleteById(atomicFile.getId());
    }

    @Test
    @Transactional
    public void searchAtomicFile() throws Exception {
        // Configure the mock search repository
        // Initialize the database
        atomicFileRepository.saveAndFlush(atomicFile);
        when(mockAtomicFileSearchRepository.search(queryStringQuery("id:" + atomicFile.getId())))
            .thenReturn(Collections.singletonList(atomicFile));

        // Search the atomicFile
        restAtomicFileMockMvc.perform(get("/api/_search/atomic-files?query=id:" + atomicFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(atomicFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].annotatedData").value(hasItem(DEFAULT_ANNOTATED_DATA)))
            .andExpect(jsonPath("$.[*].annotatedText").value(hasItem(DEFAULT_ANNOTATED_TEXT)));
    }
}
